# OSX CI Release Build Pipeline

Because Gitlab CI doesn't provide shared runners that run OSX at the moment,
an external server has to be used.

For this repository, OSX binaries are built on a Mac Mini hosted by MacStadium.

## Setup instructions

### Install homebrew

Homebrew is used to install the required dependencies:

```shell
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

### Install Opam and jq

```shell
$ brew install opam jq
```

### Install Rust

Follow Tezos' [instructions](https://tezos.gitlab.io/introduction/howtoget.html#install-rust):

```shell
$ curl https://sh.rustup.rs/rustup-init.sh -O
$ chmod +x rustup-init.sh
$ ./rustup-init.sh --profile minimal --default-toolchain 1.44.0 -y
```

### Configure Gitlab's CI runner

Follow Gitlab's [instructions](https://docs.gitlab.com/runner/install/osx.html#installation).

```shell
$ sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64"
$ sudo chmod +x /usr/local/bin/gitlab-runner
```

Register the runner:

```shell
$ /usr/local/bin/gitlab-runner register

Please enter the gitlab-ci coordinator URL (e.g.https://gitlab.com )
htps://gitlab.com

Please enter the gitlab-ci token for this runner
xxx # Obtained from Settings > CI/CD -> Runnerssection in Gitlab

Please enter the gitlab-ci description for thisrunner
[hostname] macos-runner

Please enter the gitlab-ci tags for this runner(comma separated):
macos # Important, this tag will be used to filter the OSX builder

Please enter the executor: ssh, docker+machine,docker-ssh+machine, kubernetes, docker, parallels,virtualbox, docker-ssh, shell:
shell # In OSX the binaries are built directly on shell
```

Install and start:

```shell
$ /usr/local/bin/gitlab-runner install
$ /usr/local/bin/gitlab-runner start
```

Then trigger a build to verify that it is working.