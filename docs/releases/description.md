# Tezos release artifacts

Here you will find all publicly available release artifacts.

* `libtezos-ffi-<os-distribution>.so` compiled linux library

* `libtezos-ffi-<os-distribution>.so.sha256` file with checksum for compiled linux library

* `libtezos-ffi-macos.dylib` compiled MacOS library

* `libtezos-ffi-macos.dylib.sha256` file with checksum for compiled MacOS library

* `libtezos-ffi-distribution-summary.json`
  - contains all generated distributions (name, link, sha256)
  - download this file and replace in TezEdge sources