#!/usr/bin/env bash
TAG_NAME="$1"
PROJECT_ID="$2"
PROJECT_URL="$3"
RELEASE_FILE_NAME="$4"
RELEASE_FILE_PATH="$5"
PRIVATE_TOKEN="$6"

if [ "$6" == "" ]; then
    echo "Missing parameter! Parameters are TAG_NAME, PROJECT_ID, PROJECT_URL, RELEASE_FILE_NAME, RELEASE_FILE_PATH and PRIVATE_TOKEN.";
    exit 1;
fi

if [ ! -f "$RELEASE_FILE_PATH" ]; then
    echo "Release file $RELEASE_FILE_PATH was not found"
    exit 1
fi

# Upload release file(s)
RELEASE_FILE_URL=$(curl --request POST --header "Private-Token: ${PRIVATE_TOKEN}" --form "file=@${RELEASE_FILE_PATH}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads" | jq -jr ".url")
RELEASE_FILE_DOWNLOAD_LINK="${PROJECT_URL}${RELEASE_FILE_URL}"
echo "Release file download link -- $RELEASE_FILE_DOWNLOAD_LINK"

curl --request POST \
     --header "Private-Token: ${PRIVATE_TOKEN}" \
     --data name="${RELEASE_FILE_NAME}" \
     --data url="${RELEASE_FILE_DOWNLOAD_LINK}" \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases/${TAG_NAME}/assets/links"

exit 0
