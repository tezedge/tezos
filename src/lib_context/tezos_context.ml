module Context_binary = Tezos_irmin_context.Context_binary
module Context = Tezos_tezedge_context.Xcontext
module Context_dump = Tezos_irmin_context.Context_dump

module Irmin_context = Tezos_tezedge_context.Icontext
module Tezedge_context = Tezos_tezedge_context.Tcontext
