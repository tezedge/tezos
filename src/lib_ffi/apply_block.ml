open Tezos_base__TzPervasives
open Ffi_errors

type apply_block_request = {
  chain_id : Chain_id.t;
  block_header : Block_header.t;
  pred_header : Block_header.t;
  max_operations_ttl : int;
  operations : Operation.t list list;
  (* context associated to the predecessor block *)
  predecessor_block_metadata_hash : Block_metadata_hash.t option;
  (* hash of block header metadata of the predecessor block *)
  predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option;
}

type apply_block_response = Validation_helpers.apply_block_response

let apply (block_hash : Block_hash.t)
    (apply_block_request : apply_block_request) =
  let open Lwt_result_syntax in
  let {
    chain_id;
    block_header;
    operations;
    pred_header = predecessor_block_header;
    max_operations_ttl;
    predecessor_block_metadata_hash;
    predecessor_ops_metadata_hash;
  } =
    apply_block_request
  in
  let* () = assert_complete_operations block_header operations in
  (* check received data correctness *)
  let* () = assert_predecessor_ok block_header predecessor_block_header in
  let* () = assert_operations_hash_ok block_header operations in
  let predecessor_context_hash = predecessor_block_header.shell.context in
  let user_activated_upgrades =
    !Ffi_block_validation.forced_protocol_upgrades
  in
  let user_activated_protocol_overrides =
    !Ffi_block_validation.voted_protocol_overrides
  in
  let operation_metadata_size_limit = None in
  let storage = Storage_facade.global_storage in
  let* context_index = Storage_facade.context_index storage in
  let context_index = Context.Timings.wrap context_index in
  (* checkout predecesor context and get protocol from predecessor's context *)
  let* predecessor_context =
    let*! result = Context.checkout context_index predecessor_context_hash in
    match result with
    | Some predecessor_block_context -> return predecessor_block_context
    | None ->
        tzfail
          (Unknown_predecessor_context
             (Context_hash.to_b58check predecessor_context_hash))
  in
  let apply_environment =
    Ffi_block_validation.
      {
        chain_id;
        user_activated_upgrades;
        user_activated_protocol_overrides;
        operation_metadata_size_limit;
        max_operations_ttl;
        predecessor_block_header;
        predecessor_block_metadata_hash;
        predecessor_ops_metadata_hash;
        predecessor_context;
      }
  in
  (* First time we will load the cache, and then we will inherit the cache from
     the previous block *)
  let cache =
    match !Protocol_cache.cache with
    | None -> `Force_load
    | Some cache -> `Inherited (cache, predecessor_block_header.shell.context)
  in
  (* TODO: cached_result *)
  let* result =
    Ffi_block_validation.apply
      apply_environment
      ~cache
      block_hash
      block_header
      operations
  in
  Protocol_cache.cache :=
    Some
      Environment_context.Context.
        {cache = result.cache; context_hash = result.response.context_hash} ;
  return result
