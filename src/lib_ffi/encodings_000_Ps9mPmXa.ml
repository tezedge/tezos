open Tezos_base__TzPervasives
open Encodings

let protocol_hash =
  Protocol_hash.of_b58check_exn
    "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P"

let explore register =
  register
    protocol_hash
    [
      record ["protocol"] Protocol_hash.encoding;
      record ["test_chain"] Test_chain_status.encoding;
      record ["data"; "version"] Data_encoding.Variable.string;
      record ["data"; "protocol_parameters"] Data_encoding.Variable.bytes;
    ] ;
  ()
