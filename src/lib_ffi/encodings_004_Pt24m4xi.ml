open Tezos_base__TzPervasives
open Encodings

let protocol_hash =
  Protocol_hash.of_b58check_exn
    "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"

let explore register =
  let open Tezos_protocol_004_Pt24m4xi in
  let open Protocol in
  register
    protocol_hash
    [
      record ["protocol"] Protocol_hash.encoding;
      record ["test_chain"] Test_chain_status.encoding;
      record ["data"; "version"] Data_encoding.Variable.string;
      record ["data"; "protocol_parameters"] Data_encoding.Variable.bytes;
      record ["data"; "v1"; "first_level"] Raw_level_repr.encoding;
      record ["data"; "v1"; "constants"] Parameters_repr.constants_encoding;
      record ["data"; "commitments"; o; o; o; o; o; o] Tez_repr.encoding;
      record ["data"; "last_block_priority"] Data_encoding.uint16;
      record
        ["data"; "active_delegates_with_rolls"; o; o; o; o; o; o; o]
        Data_encoding.Variable.string;
      record
        ["data"; "delegates_with_frozen_balance"; o; o; o; o; o; o; o; o]
        Data_encoding.Variable.string;
      record
        ["data"; "delegates"; o; o; o; o; o; o; o]
        Data_encoding.Variable.string;
      record ["data"; "rolls"; "next"] Roll_repr.encoding;
      record ["data"; "rolls"; "limbo"] Roll_repr.encoding;
      record
        ["data"; "rolls"; "owner"; "current"; o; o; o]
        Signature.Public_key.encoding;
      record ["data"; "rolls"; "index"; o; o; o; "successor"] Roll_repr.encoding;
      record ["data"; "cycle"; o; "random_seed"] Seed_repr.seed_encoding;
      record ["data"; "cycle"; o; "roll_snapshot"] Data_encoding.uint16;
      record ["data"; "cycle"; o; "last_roll"; o] Roll_repr.encoding;
      record
        ["data"; "ramp_up"; "rewards"; o]
        (Data_encoding.tup2 Tez_repr.encoding Tez_repr.encoding);
      record
        ["data"; "ramp_up"; "deposits"; o]
        (Data_encoding.tup2 Tez_repr.encoding Tez_repr.encoding);
      record ["data"; "votes"; "current_quorum"] Data_encoding.int32;
      record
        ["data"; "votes"; "current_period_kind"]
        Voting_period_repr.kind_encoding;
      record
        ["data"; "votes"; "listings"; o; o; o; o; o; o; o]
        Data_encoding.int32;
      record ["data"; "votes"; "listings_size"] Data_encoding.int32;
      record ["data"; "votes"; "participation_ema"] Data_encoding.int32;
      record
        ["data"; "votes"; "ballots"; o; o; o; o; o; o; o]
        Vote_repr.ballot_encoding;
      record ["data"; "votes"; "current_proposal"] Protocol_hash.encoding;
      record
        ["data"; "votes"; "proposals_count"; o; o; o; o; o; o; o]
        Data_encoding.uint16;
      record
        ["data"; "votes"; "proposals"; o; o; o; o; o; o; o; o; o; o; o; o; o]
        Data_encoding.Variable.string;
      record ["data"; "contracts"; "global_counter"] Data_encoding.z;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "counter"]
        Data_encoding.z;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "balance"]
        Tez_repr.encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "manager"]
        Manager_repr.encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "spendable"]
        Data_encoding.Variable.string;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "delegate"]
        Signature.Public_key_hash.encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "change"]
        Tez_repr.encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "roll_list"]
        Roll_repr.encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "inactive_delegate"]
        Data_encoding.Variable.string;
      record
        [
          "data";
          "contracts";
          "index";
          o;
          o;
          o;
          o;
          o;
          o;
          o;
          "delegate_desactivation";
        ]
        Cycle_repr.encoding;
      record
        [
          "data";
          "contracts";
          "index";
          o;
          o;
          o;
          o;
          o;
          o;
          o;
          "frozen_balance";
          o;
          "deposits";
        ]
        Tez_repr.encoding;
      record
        [
          "data";
          "contracts";
          "index";
          o;
          o;
          o;
          o;
          o;
          o;
          o;
          "frozen_balance";
          o;
          "fees";
        ]
        Tez_repr.encoding;
      record
        [
          "data";
          "contracts";
          "index";
          o;
          o;
          o;
          o;
          o;
          o;
          o;
          "frozen_balance";
          o;
          "rewards";
        ]
        Tez_repr.encoding;
      record
        [
          "data";
          "contracts";
          "index";
          o;
          o;
          o;
          o;
          o;
          o;
          o;
          "delegated";
          o;
          o;
          o;
          o;
          o;
          o;
          o;
        ]
        Data_encoding.Variable.string;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "len"; "code"]
        Data_encoding.int31;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "data"; "code"]
        Script_repr.lazy_expr_encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "len"; "storage"]
        Data_encoding.int31;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "data"; "storage"]
        Script_repr.lazy_expr_encoding;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "paid_bytes"]
        Data_encoding.z;
      record
        ["data"; "contracts"; "index"; o; o; o; o; o; o; o; "used_bytes"]
        Data_encoding.z;
    ] ;
  ()
