open Tezos_base__TzPervasives
open Encodings

type value_as_json = string

let encodings_registry = ref EncodingMap.empty

let resolve_json_data (protocol_hash : Protocol_hash.t) (key : string list)
    (data : Bytes.t) =
  if Bytes.length data > 0 then
    match EncodingMap.find protocol_hash !encodings_registry with
    | None -> None
    | Some protocol_encodings -> (
        match
          List.find_opt
            (fun record -> key_matches_pattern record key)
            protocol_encodings
        with
        | None -> None
        | Some (Record {encoding; _}) ->
            let json =
              Ffi_utils.to_json
                ~newline:false
                ~minify:true
                encoding
                (Ffi_utils.decode_from_mbytes encoding data)
            in
            Some json)
  else None

let join key = String.concat ", " key

let resolve_value_as_json (protocol_hash : Protocol_hash.t option)
    (key : string list) (data : Bytes.t) : value_as_json option =
  try
    match protocol_hash with
    | None -> None
    | Some protocol_hash -> (
        match resolve_json_data protocol_hash key data with
        | None ->
            Ffi_logger.ffi_log_error ~msg:(fun () ->
                Printf.sprintf
                  "No encoding configured for protocol: %s and key: %s data: %s"
                  (Protocol_hash.to_b58check protocol_hash)
                  (join key)
                  (Ffi_utils.mbytes_to_hex_string data)) ;
            None
        | Some json -> Some json)
  with e ->
    let error_msg = Printexc.to_string e
    and stack = Printexc.get_backtrace () in
    let formatted_msg =
      Printf.sprintf
        "error_msg: '%s' stacktrace: '%s' for key: '%s' data: '%s'"
        error_msg
        stack
        (join key)
        (Ffi_utils.mbytes_to_hex_string data)
    in
    Ffi_logger.ffi_log_error ~msg:(fun () ->
        Printf.sprintf
          "resolve_value_as_json failed, but we just return empty string: %s"
          formatted_msg) ;
    None

let register protocol_hash encodings =
  encodings_registry :=
    EncodingMap.add protocol_hash encodings !encodings_registry ;
  ()

let explore () =
  Encodings_genesis.explore register ;
  Encodings_genesis_Ps6mwMrF.explore register ;
  Encodings_genesis_PtBMwNZT.explore register ;
  Encodings_genesis_PtYuensg.explore register ;
  Encodings_000_Ps9mPmXa.explore register ;
  Encodings_001_PtCJ7pwo.explore register ;
  Encodings_002_PsYLVpVv.explore register ;
  Encodings_003_PsddFKi3.explore register ;
  Encodings_004_Pt24m4xi.explore register ;
  Encodings_005_PsBabyM1.explore register ;
  Encodings_005_PsBABY5H.explore register ;
  Encodings_006_PsCARTHA.explore register ;
  Encodings_007_PsDELPH1.explore register ;
  Encodings_008_PtEdoTez.explore register ;
  Encodings_008_PtEdo2Zk.explore register ;
  Encodings_009_PsFLoren.explore register ;
  Encodings_010_PtGRANAD.explore register ;
  Encodings_011_PtHangz2.explore register ;
  Encodings_012_Psithaca.explore register ;
  (* TODO: another encoding for another protocols or to change to DataEncoding.registration *)
  ()
