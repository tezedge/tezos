type hash

type json_encode_apply_block_result_metadata_request = {
  context_hash : Context_hash.t;
  metadata_bytes : bytes;
  max_operations_ttl : int;
  protocol_hash : Protocol_hash.t;
  next_protocol_hash : Protocol_hash.t;
}

type json_encode_apply_block_operations_metadata_request = {
  chain_id : Chain_id.t;
  all_operations : Operation.t list list;
  all_operations_metadata_bytes : bytes list list;
  protocol_hash : Protocol_hash.t;
  next_protocol_hash : Protocol_hash.t;
}

type init_protocol_context_result = {
  known_protocols : Protocol_hash.t list;
  genesis_context_hash : Context_hash.t option;
}

(* Initializes tezos context for protocol in directory. *)
val init_protocol_context :
  Ffi_config.Context.t -> init_protocol_context_result tzresult Lwt.t

(* Gets genesis json data for rpc. *)
val genesis_result_data :
  Block_explorer.commit_genesis_result_data_request ->
  Block_explorer.commit_genesis_result_data tzresult Lwt.t

(* Validates block and operations with protocol and addd header+operations to storage (context/state) *)
val apply_block :
  Apply_block.apply_block_request ->
  Apply_block.apply_block_response tzresult Lwt.t

(* Validates block_header with protocol against predecessor block *)
val begin_application :
  Validate_block.begin_application_request ->
  Validate_block.begin_application_response tzresult Lwt.t

(* Validates block_header's protocol_data with protocol encoding *)
val assert_encoding_for_protocol_data :
  Protocol_hash.t -> bytes -> unit tzresult

(* Initializes validation state for a new prevalidator *)
val begin_construction :
  Validate_block.begin_construction_request ->
  Validate_block.prevalidator_wrapper tzresult Lwt.t

(* Validation operation with current prevalidator/protocol *)
val validate_operation :
  Validate_block.prevalidator_request ->
  Validate_block.validate_operation_response tzresult Lwt.t

(* Pre-filter operation with current prevalidator/protocol *)
val pre_filter_operation :
  Validate_block.prevalidator_request ->
  Validate_block.pre_filter_operation_response tzresult Lwt.t

(*
    Ocaml runtime configuration
    input
        bool - log enabling
*)
val change_runtime_configuration : Ffi_config.RuntimeConfiguration.t -> unit

(*
    hash - protocol hash (Protocol_hash.t)
    string list - context key
    bytes - MBytes data
*)
val decode_context_data : hash -> string list -> bytes -> string option tzresult

val compute_path :
  Operation_hash.t list list -> Operation_list_list_hash.path list tzresult

(*
    (shell rpc)
    Validation preapply operations like block_services/block_directory -> module Preapply -> let operations
*)
val helpers_preapply_operations :
  Ffi_rpc_structs.protocol_rpc_request ->
  Ffi_rpc_structs.helpers_preapply_response tzresult Lwt.t

(*
    (shell rpc)
    Validation preapply block like: block_services/block_directory -> module Preapply -> let block
*)
val helpers_preapply_block :
  Ffi_rpc_structs.helpers_preapply_block_request ->
  Ffi_rpc_structs.helpers_preapply_response tzresult Lwt.t

(* JSON encoding *)

val apply_block_result_metadata :
  json_encode_apply_block_result_metadata_request -> string tzresult Lwt.t

val apply_block_operations_metadata :
  json_encode_apply_block_operations_metadata_request -> string tzresult Lwt.t
