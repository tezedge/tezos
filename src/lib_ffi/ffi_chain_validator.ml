(* This file is copied from src/lib_shell/chain_validator.ml *)

(* according to chain_validator.on_request *)
let on_success_block_validated (enable_testchain_scenario : bool) =
  let open Lwt_result_syntax in
  (* e.g. shell, is doing: Chain.set_head chain block, this should be handled in client, like check fitnees and other checks accepted_head *)

  (* if configured, let's handle test chain scenario *)
  if enable_testchain_scenario then
    (* TODO:TE-123 real impl by reimplement chain_validator.ml -> may_switch_test_chain  *)
    (* TODO: reimplement chain_validator.may_switch_test_chain on shell level *)
    tzfail
      (Ffi_errors.Ffi_call_error
         "TODO:TE-123 - Switch test chain scenario is not yet supported!")
  else return_unit
