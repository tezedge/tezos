open Ffi_utils

module Cstruct_buffer_bytes_converter = struct
  type t = Cstruct.buffer

  let to_mbytes (value : t) = Cstruct.to_bytes @@ Cstruct.of_bigarray value

  let from_mbytes value : t = Cstruct.to_bigarray @@ Cstruct.of_bytes value

  let empty : t = Cstruct.to_bigarray @@ Cstruct.empty
end

module Bytes_bytes_converter = struct
  type t = Bytes.t

  let to_mbytes (value : t) = value

  let from_mbytes value : t = value

  let empty : t = Bytes.empty
end

module Hash_converter = struct
  type t = Bytes.t

  let to_mbytes (value : t) = Bytes_bytes_converter.to_mbytes value

  let from_mbytes value : t = Bytes_bytes_converter.from_mbytes value

  let empty : t = Bytes_bytes_converter.empty

  let to_chain_id chain_id = Chain_id.of_bytes_exn @@ to_mbytes chain_id

  let of_chain_id chain_id = from_mbytes @@ Chain_id.to_bytes chain_id

  let to_block_hash block_hash = Block_hash.of_bytes_exn @@ to_mbytes block_hash

  let of_block_hash block_hash = from_mbytes @@ Block_hash.to_bytes block_hash

  let to_operation_hash operation_hash =
    Operation_hash.of_bytes_exn @@ to_mbytes operation_hash

  let of_operation_hash operation_hash =
    from_mbytes @@ Operation_hash.to_bytes operation_hash

  let to_context_hash context_hash =
    Context_hash.of_bytes_exn @@ to_mbytes context_hash

  let of_context_hash context_hash =
    from_mbytes @@ Context_hash.to_bytes context_hash

  let to_protocol_hash protocol_hash =
    Protocol_hash.of_bytes_exn @@ to_mbytes protocol_hash

  let of_protocol_hash protocol_hash =
    from_mbytes @@ Protocol_hash.to_bytes protocol_hash
end

let genesis_to_string genesis =
  let (genesis_time, genesis_block, genesis_protocol) = genesis in
  Printf.sprintf
    "\n    time: %s\n    block: %s\n    protocol: %s"
    genesis_time
    genesis_block
    genesis_protocol

let protocol_overrides_to_string protocol_overrides =
  let (forced_protocol_upgrades, voted_protocol_overrides) =
    protocol_overrides
  in
  let forced_protocol_upgrades =
    List.map
      (fun (a, b) -> Printf.sprintf "%s -> %s" (Int32.to_string a) b)
      forced_protocol_upgrades
  in
  let voted_protocol_overrides =
    List.map
      (fun (a, b) -> Printf.sprintf "%s -> %s" a b)
      voted_protocol_overrides
  in
  Printf.sprintf
    "forced_protocol_upgrades: [%s], voted_protocol_overrides: [%s]"
    (String.concat ", " forced_protocol_upgrades)
    (String.concat ", " voted_protocol_overrides)

(*////////////////////*)
(*// for operations //*)
(*////////////////////*)
let operations_dump_to_string ~to_mbytes operations_list_list =
  let operations_list_list_as_string =
    List.mapi
      (fun list_idx operations_list ->
        let ops_as_string =
          List.mapi
            (fun op_idx operation ->
              let operation_as_hex =
                mbytes_to_hex_string @@ to_mbytes operation
              in
              Printf.sprintf
                "\n    op:[%i]\n         [%i]: %s"
                list_idx
                op_idx
                operation_as_hex)
            operations_list
        in
        if List.length ops_as_string > 0 then String.concat "" ops_as_string
        else Printf.sprintf "\n    op:[%i] - empty" list_idx)
      operations_list_list
  in
  String.concat "" operations_list_list_as_string

let operations_list_list_from_bytes ~to_mbytes operations_list_list =
  List.map
    (fun operations_list ->
      List.map
        (fun operation -> Operation.of_bytes_exn @@ to_mbytes operation)
        operations_list)
    operations_list_list

let operations_list_list_to_bytes ~from_mbytes
    (operations_list_list : Operation.t list list) =
  List.map
    (fun operations_list ->
      List.map
        (fun operation -> from_mbytes @@ Operation.to_bytes operation)
        operations_list)
    operations_list_list

let operations_from_bytes ~to_mbytes operations_list_list =
  Array.map
    (fun operations_list ->
      Array.map
        (fun operation -> Operation.of_bytes_exn @@ to_mbytes operation)
        operations_list)
    operations_list_list

let operations_to_bytes ~from_mbytes
    (operations_list_list : Operation.t array array) =
  Array.map
    (fun operations_list ->
      Array.map
        (fun operation -> from_mbytes @@ Operation.to_bytes operation)
        operations_list)
    operations_list_list
