type error += Ffi_call_error of string

type error += Ffi_call_exception of string

type error += Predecessor_mismatch of string

type error += Unknown_predecessor_context of string

type error += Unknown_context of string

type error += Apply_error of string

type error += Inconsistent_operations_hash of string

type error += Incomplete_operations of {expected : int; actual : int}

type error += Unavailable_protocol of string

type error +=
  | Context_hash_result_mismatch of {
      expected : string;
      actual : string;
      cache : bool;
    }

let () =
  register_error_kind
    `Permanent
    ~id:"ffi.call_error"
    ~title:"FFI call error"
    ~description:"FFI call failed"
    ~pp:(fun ppf description ->
      Format.fprintf ppf "FFI call failed: %s." description)
    Data_encoding.(obj1 (req "description" string))
    (function Ffi_call_error description -> Some description | _ -> None)
    (fun description -> Ffi_call_error description) ;
  register_error_kind
    `Permanent
    ~id:"ffi.call_exception"
    ~title:"FFI call exception"
    ~description:"FFI call failed with exception"
    ~pp:(fun ppf description ->
      Format.fprintf ppf "FFI call failed with exception: %s." description)
    Data_encoding.(obj1 (req "description" string))
    (function Ffi_call_exception description -> Some description | _ -> None)
    (fun description -> Ffi_call_exception description) ;
  register_error_kind
    `Permanent
    ~id:"ffi.predecessor_mismatch"
    ~title:"Predecessor mismatch"
    ~description:"Predecessor does not match"
    ~pp:(fun ppf hash ->
      Format.fprintf ppf "Predecessor does not match: %s." hash)
    Data_encoding.(obj1 (req "hash" string))
    (function Predecessor_mismatch hash -> Some hash | _ -> None)
    (fun hash -> Predecessor_mismatch hash) ;
  register_error_kind
    `Permanent
    ~id:"ffi.unknown_predecessor_context"
    ~title:"Unknown predecessor context"
    ~description:"Unknown predecessor context"
    ~pp:(fun ppf hash ->
      Format.fprintf
        ppf
        "Unknown predecessor context - try to apply predecessor at first \
         message: %s."
        hash)
    Data_encoding.(obj1 (req "hash" string))
    (function Unknown_predecessor_context hash -> Some hash | _ -> None)
    (fun hash -> Unknown_predecessor_context hash) ;
  register_error_kind
    `Permanent
    ~id:"ffi.unknown_context"
    ~title:"Unknown context hash"
    ~description:"Unknown context hash"
    ~pp:(fun ppf hash -> Format.fprintf ppf "Unknown context_hash: %s." hash)
    Data_encoding.(obj1 (req "hash" string))
    (function Unknown_context hash -> Some hash | _ -> None)
    (fun hash -> Unknown_context hash) ;
  register_error_kind
    `Permanent
    ~id:"ffi.apply_error"
    ~title:"Apply error"
    ~description:"Apply error"
    ~pp:(fun ppf description ->
      Format.fprintf ppf "Apply error: %s." description)
    Data_encoding.(obj1 (req "description" string))
    (function Apply_error description -> Some description | _ -> None)
    (fun description -> Apply_error description) ;
  register_error_kind
    `Permanent
    ~id:"ffi.inconsistent_operations_hash"
    ~title:"Inconsistent operations hash"
    ~description:"Inconsistent operations hash"
    ~pp:(fun ppf description ->
      Format.fprintf ppf "Inconsistent operations hash: %s." description)
    Data_encoding.(obj1 (req "description" string))
    (function
      | Inconsistent_operations_hash description -> Some description | _ -> None)
    (fun description -> Inconsistent_operations_hash description) ;
  register_error_kind
    `Permanent
    ~id:"ffi.incomplete_operations"
    ~title:"Incomplete operations"
    ~description:"Incomplete operations"
    ~pp:(fun ppf (expected, actual) ->
      Format.fprintf
        ppf
        "Incomplete operations: expected=%d actual=%d."
        expected
        actual)
    Data_encoding.(obj2 (req "expected" int31) (req "actual" int31))
    (function
      | Incomplete_operations {expected; actual} -> Some (expected, actual)
      | _ -> None)
    (fun (expected, actual) -> Incomplete_operations {expected; actual}) ;
  register_error_kind
    `Permanent
    ~id:"ffi.unavailable_protocol"
    ~title:"Unavailable protocol"
    ~description:"Unavailable protocol"
    ~pp:(fun ppf hash -> Format.fprintf ppf "Unavailable protocol: %s." hash)
    Data_encoding.(obj1 (req "hash" string))
    (function Unavailable_protocol hash -> Some hash | _ -> None)
    (fun hash -> Unavailable_protocol hash) ;
  register_error_kind
    `Permanent
    ~id:"ffi.context_hash_result_mismatch"
    ~title:"Result context hash mismatch"
    ~description:"Result context hash mismatch"
    ~pp:(fun ppf (expected, actual, cache) ->
      Format.fprintf
        ppf
        "Result context hash mismatch: expected=%s actual=%s cache=%b."
        expected
        actual
        cache)
    Data_encoding.(
      obj3 (req "expected" string) (req "actual" string) (req "cache" bool))
    (function
      | Context_hash_result_mismatch {expected; actual; cache} ->
          Some (expected, actual, cache)
      | _ -> None)
    (fun (expected, actual, cache) ->
      Context_hash_result_mismatch {expected; actual; cache}) ;
  ()

let assert_complete_operations (block_header : Block_header.t) operations =
  let expected = block_header.shell.validation_passes in
  let actual = List.length operations in
  fail_unless (expected = actual) (Incomplete_operations {expected; actual})

let assert_predecessor_ok (block : Block_header.t) (pred : Block_header.t) =
  let open Lwt_result_syntax in
  if Block_hash.equal block.shell.predecessor pred.shell.predecessor then
    (* special case for genesis, which has genesis as predecessor *)
    return_unit
  else if
    not (Block_hash.equal block.shell.predecessor (Block_header.hash pred))
  then
    tzfail
      (Predecessor_mismatch
         (Printf.sprintf
            "Predecessor mismatch - %s vs %s"
            (Block_hash.to_b58check block.shell.predecessor)
            (Block_hash.to_b58check pred.shell.predecessor)))
  else return_unit

let display_operation_hashes ops_hashes =
  let display_hashes_list operation_hashes =
    let hash_strings = List.map Operation_hash.to_b58check operation_hashes in
    "[" ^ String.concat ", " hash_strings ^ "]"
  in
  String.concat ", " (List.map display_hashes_list ops_hashes)

let assert_operations_hash_ok (block_header : Block_header.t)
    (operations : Operation.t list list) =
  let open Lwt_result_syntax in
  let operation_hashes =
    List.map (List.map Tezos_base.Operation.hash) operations
  in
  let computed_hash =
    Operation_list_list_hash.compute
      (List.map Operation_list_hash.compute operation_hashes)
  in
  let are_oph_equal =
    Operation_list_list_hash.equal
      computed_hash
      block_header.shell.operations_hash
  in
  if are_oph_equal then return_unit
  else
    tzfail
      (Inconsistent_operations_hash
         (Printf.sprintf
            "expected: %s vs %s. Operation hashes: expected %s"
            (Operation_list_list_hash.to_b58check
               block_header.shell.operations_hash)
            (Operation_list_list_hash.to_b58check computed_hash)
            (display_operation_hashes operation_hashes)))

let failwith msg =
  let open Lwt_result_syntax in
  tzfail (Ffi_call_error msg)
