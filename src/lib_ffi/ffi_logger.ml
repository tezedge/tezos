(* TODO: remove this, use tezos log module instead *)

let log_enabled = ref false

let set_log_enabled value = log_enabled := value

let now = Time.System.(now () |> to_notation)

let ffi_log ~(msg : unit -> string) =
  if !log_enabled then print_endline (now ^ " DEBG (ffi) " ^ msg ()) else ()

let ffi_log_error ~(msg : unit -> string) =
  if !log_enabled then print_endline (now ^ " ERRO (ffi) " ^ msg ()) else ()
