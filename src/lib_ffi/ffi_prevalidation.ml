(* The contents on this file were copied and adapted from lib_shell/prevalidation.ml *)

open Tezos_shell_services.Validation_errors

type 'protocol_operation operation = {
  hash : Operation_hash.t;
  raw : Operation.t;
  protocol : 'protocol_operation;
  count_successful_prechecks : int;
}

module type T = sig
  type protocol_operation

  type operation_receipt

  type validation_state

  type t

  val parse :
    Operation_hash.t -> Operation.t -> protocol_operation operation tzresult

  val increment_successful_precheck :
    protocol_operation operation -> protocol_operation operation

  val create :
    ?protocol_data:Bytes.t ->
    predecessor_header:Block_header.t ->
    predecessor_hash:Block_hash.t ->
    predecessor_context:Context.t ->
    chain_id:Chain_id.t ->
    timestamp:Time.Protocol.t ->
    unit ->
    t tzresult Lwt.t

  type result =
    | Applied of t * operation_receipt
    | Branch_delayed of tztrace
    | Branch_refused of tztrace
    | Refused of tztrace
    | Outdated of tztrace

  val apply_operation : t -> protocol_operation operation -> result Lwt.t

  val validation_state : t -> validation_state

  val pp_result : Format.formatter -> result -> unit

  module Internal_for_tests : sig
    val to_applied :
      t -> (protocol_operation operation * operation_receipt) list
  end
end

(** Doesn't depend on heavy [Registered_protocol.T] for testability. *)
let safe_binary_of_bytes (encoding : 'a Data_encoding.t) (bytes : bytes) :
    'a tzresult =
  let open Result_syntax in
  match Data_encoding.Binary.of_bytes_opt encoding bytes with
  | None -> tzfail Parse_error
  | Some protocol_data -> return protocol_data

module MakeAbstract (Proto : Tezos_protocol_environment.PROTOCOL) :
  T
    with type protocol_operation = Proto.operation
     and type operation_receipt = Proto.operation_receipt
     and type validation_state = Proto.validation_state = struct
  type protocol_operation = Proto.operation

  type operation_receipt = Proto.operation_receipt

  type validation_state = Proto.validation_state

  type t = {
    state : Proto.validation_state;
    applied : (protocol_operation operation * Proto.operation_receipt) list;
  }

  type result =
    | Applied of t * Proto.operation_receipt
    | Branch_delayed of tztrace
    | Branch_refused of tztrace
    | Refused of tztrace
    | Outdated of tztrace

  let parse_unsafe (proto : bytes) : Proto.operation_data tzresult =
    safe_binary_of_bytes Proto.operation_data_encoding proto

  let parse hash (raw : Operation.t) =
    let open Result_syntax in
    let size = Data_encoding.Binary.length Operation.encoding raw in
    if size > Proto.max_operation_data_length then
      tzfail (Oversized_operation {size; max = Proto.max_operation_data_length})
    else
      let+ protocol_data = parse_unsafe raw.proto in
      {
        hash;
        raw;
        protocol = {Proto.shell = raw.Operation.shell; protocol_data};
        (* When an operation is parsed, we assume that it has never been
           successfully prechecked. *)
        count_successful_prechecks = 0;
      }

  let increment_successful_precheck op =
    (* We avoid {op with ...} to get feedback from the compiler if the record
       type is extended/modified in the future. *)
    {
      hash = op.hash;
      raw = op.raw;
      protocol = op.protocol;
      count_successful_prechecks = op.count_successful_prechecks + 1;
    }

  let create ?protocol_data ~predecessor_header ~predecessor_hash
      ~predecessor_context ~chain_id ~timestamp () =
    (* The prevalidation module receives input from the system byt handles
       protocol values. It translates timestamps here. *)
    let open Lwt_result_syntax in
    let {
      Block_header.shell =
        {
          fitness = predecessor_fitness;
          timestamp = predecessor_timestamp;
          level = predecessor_level;
          _;
        };
      _;
    } =
      predecessor_header
    in
    let*! predecessor_context =
      Ffi_block_validation.update_testchain_status
        predecessor_context
        ~predecessor_hash
        timestamp
    in
    let* protocol_data =
      match protocol_data with
      | None -> return_none
      | Some protocol_data -> (
          match
            Data_encoding.Binary.of_bytes_opt
              Proto.block_header_data_encoding
              protocol_data
          with
          | None -> failwith "Invalid block header"
          | Some protocol_data -> return_some protocol_data)
    in
    let predecessor_context =
      Shell_context.wrap_disk_context predecessor_context
    in
    let* state =
      Proto.begin_construction
        ~chain_id
        ~predecessor_context
        ~predecessor_timestamp
        ~predecessor_fitness
        ~predecessor_level
        ~predecessor:predecessor_hash
        ~timestamp
        ?protocol_data
        ~cache:`Lazy
        ()
    in
    return {state; applied = []}

  let apply_operation pv op =
    let open Lwt_syntax in
    let+ r = protect (fun () -> Proto.apply_operation pv.state op.protocol) in
    match r with
    | Ok (state, receipt) -> (
        let pv = {state; applied = (op, receipt) :: pv.applied} in
        match
          Data_encoding.Binary.(
            of_bytes_exn
              Proto.operation_receipt_encoding
              (to_bytes_exn Proto.operation_receipt_encoding receipt))
        with
        | receipt -> Applied (pv, receipt)
        | exception exn ->
            Refused
              [Validation_errors.Cannot_serialize_operation_metadata; Exn exn])
    | Error trace -> (
        match classify_trace trace with
        | Branch -> Branch_refused trace
        | Permanent -> Refused trace
        | Temporary -> Branch_delayed trace
        | Outdated -> Outdated trace)

  let validation_state {state; _} = state

  let pp_result ppf =
    let open Format in
    function
    | Applied _ -> pp_print_string ppf "applied"
    | Branch_delayed err -> fprintf ppf "branch delayed (%a)" pp_print_trace err
    | Branch_refused err -> fprintf ppf "branch refused (%a)" pp_print_trace err
    | Refused err -> fprintf ppf "refused (%a)" pp_print_trace err
    | Outdated err -> fprintf ppf "outdated (%a)" pp_print_trace err

  module Internal_for_tests = struct
    let to_applied {applied; _} = applied
  end
end

module Make (Proto : Tezos_protocol_environment.PROTOCOL) :
  T
    with type protocol_operation = Proto.operation
     and type operation_receipt = Proto.operation_receipt
     and type validation_state = Proto.validation_state =
  MakeAbstract (Proto)
