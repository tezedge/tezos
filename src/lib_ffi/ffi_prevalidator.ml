(* The contents on this file were copied and adapted from lib_shell/prevalidator.ml *)

module Prevalidation = Ffi_prevalidation
module Prevalidator_classification = Ffi_prevalidator_classification
module Prevalidator_pending_operations = Ffi_prevalidator_pending_operations

type limits = {disable_precheck : bool}

let default_limits =
  {
    disable_precheck =
      true (* [tezedge]: set to true because the shell does it already *);
  }

module Name = struct
  type t = Chain_id.t * Protocol_hash.t

  let encoding = Data_encoding.tup2 Chain_id.encoding Protocol_hash.encoding

  let base = ["prevalidator"]

  let pp fmt (chain_id, proto_hash) =
    Format.fprintf
      fmt
      "%a:%a"
      Chain_id.pp_short
      chain_id
      Protocol_hash.pp_short
      proto_hash

  let equal (c1, p1) (c2, p2) =
    Chain_id.equal c1 c2 && Protocol_hash.equal p1 p2
end

type parameters = {limits : limits}

module Classification = Prevalidator_classification

(** This module encapsulates pending operations to maintain them in two
       different data structure and avoid coslty repetitive convertions when
       handling batches in [classify_pending_operations]. *)
module Pending_ops = Prevalidator_pending_operations

module type T = sig
  (** Type instantiated by {!Filter.Mempool.state}. *)
  type filter_state

  (** Type instantiated by {!Filter.Mempool.config}. *)
  type filter_config

  (** Similar to the type [operation] from the protocol,
         see {!Tezos_protocol_environment.PROTOCOL} *)
  type protocol_operation

  (** Type instantiated by {!Prevalidation.t} *)
  type prevalidation_t

  val name : Name.t

  type types_state = {
    mutable filter_state : filter_state;
        (** Internal state of the filter in the plugin *)
    mutable validation_state : prevalidation_t tzresult;
    mutable filter_config : filter_config;
  }

  (* For TezEdge *)

  type 'operation_data operation = 'operation_data Prevalidation.operation

  val parse :
    Operation_hash.t -> Operation.t -> protocol_operation operation tzresult

  val launch :
    ?protocol_data:Bytes.t ->
    chain_id:Chain_id.t ->
    predecessor_context:Context.t ->
    predecessor_header:Block_header.t ->
    predecessor_hash:Block_hash.t ->
    unit ->
    unit tzresult Lwt.t

  val validate_operation :
    protocol_operation operation ->
    (Classification.classification
    * bool
    * (Operation_hash.t * Classification.error_classification) option)
    tzresult
    Lwt.t

  val pre_filter_operation :
    protocol_operation operation ->
    [`Drop | Pending_ops.priority] tzresult Lwt.t

  val operation_data_json : protocol_operation operation -> string tzresult
end

module type ARG = sig
  val limits : limits

  val chain_id : Chain_id.t
end

type t = (module T)

module Make
    (Filter : Prevalidator_filters.FILTER)
    (Arg : ARG)
    (Prevalidation_t : Prevalidation.T
                         with type validation_state =
                               Filter.Proto.validation_state
                          and type protocol_operation = Filter.Proto.operation
                          and type operation_receipt =
                               Filter.Proto.operation_receipt) :
  T
    with type filter_state = Filter.Mempool.state
     and type filter_config = Filter.Mempool.config
     and type protocol_operation = Filter.Proto.operation
     and type prevalidation_t = Prevalidation_t.t = struct
  type filter_state = Filter.Mempool.state

  type filter_config = Filter.Mempool.config

  type protocol_operation = Filter.Proto.operation

  type prevalidation_t = Prevalidation_t.t

  let name = (Arg.chain_id, Filter.Proto.hash)

  type 'operation_data operation = 'operation_data Prevalidation.operation

  type types_state = {
    mutable filter_state : filter_state;
    mutable validation_state : prevalidation_t tzresult;
    mutable filter_config : filter_config;
  }

  (* TODO: add a cache by (proto * op_hash) to avoid repeated parsing *)
  (* TODO: in the prechecker, the precheck counter is increased, replace
     the old value in the cache with the new one with increased count *)
  let parse = Prevalidation_t.parse

  let pre_filter ~filter_config ~filter_state ~validation_state
      (parsed_op : protocol_operation operation) :
      [Pending_ops.priority | `Drop] Lwt.t =
    let open Lwt_syntax in
    let validation_state_before =
      Option.map
        Prevalidation_t.validation_state
        (Option.of_result validation_state)
    in
    let* result =
      Filter.Mempool.pre_filter
        ~filter_state
        ?validation_state_before
        filter_config
        parsed_op.protocol
    in
    match result with
    | `Branch_delayed _ | `Branch_refused _ | `Refused _ | `Outdated _ ->
        return `Drop
    | `Passed_prefilter priority ->
        return (priority :> [Pending_ops.priority | `Drop])

  let post_filter ~filter_config ~filter_state ~validation_state_before
      ~validation_state_after op receipt =
    Filter.Mempool.post_filter
      filter_config
      ~filter_state
      ~validation_state_before
      ~validation_state_after
      (op, receipt)

  (* TODO: unused for now, expose to use as a fallback mechanism for when prechecking
     for a new protocol has not been re-implemented yet in the Rust shell *)
  let precheck ~disable_precheck ~filter_config ~filter_state ~validation_state
      (op : protocol_operation operation) =
    let open Lwt_syntax in
    let validation_state = Prevalidation_t.validation_state validation_state in
    if disable_precheck then Lwt.return `Undecided
    else
      let+ v =
        Filter.Mempool.precheck
          filter_config
          ~filter_state
          ~validation_state
          ~nb_successful_prechecks:op.count_successful_prechecks
          op.hash
          op.protocol
      in
      match v with
      | `Passed_precheck (filter_state, replacement) ->
          (* The [precheck] optimization triggers: no need to call the
              protocol [apply_operation]. *)
          let new_op = Prevalidation_t.increment_successful_precheck op in
          `Passed_precheck (filter_state, new_op, replacement)
      | (`Branch_delayed _ | `Branch_refused _ | `Refused _ | `Outdated _) as
        errs ->
          (* Note that we don't need to distinguish some failure cases
             of [Filter.Mempool.precheck], hence grouping them under `Fail. *)
          `Fail errs
      | `Undecided ->
          (* The caller will need to call the protocol's [apply_operation]
             function. *)
          `Undecided

  (* [classify_operation shell filter_config filter_state validation_state
      mempool op oph] allows to determine the class of a given operation.

     Once it's parsed, the operation is prechecked and/or applied in the current
     filter/validation state to determine if it could be included in a block on
     top of the current head or not. If yes, the operation is accumulated in
     the given [mempool].

     The function returns a tuple
     [(filter_state, validation_state, classification, to_reclassify)], where:
     - [filter_state] is the (possibly) updated filter_state,
     - [validation_state] is the (possibly) updated validation_state,
     - [classification] is the operation classification.
     - [to_reclassify] an optional operation to reclassify.
  *)
  let classify_operation filter_config ~filter_state ~validation_state op :
      (filter_state
      * prevalidation_t
      * Classification.classification
      * (Operation_hash.t * Classification.error_classification) option)
      Lwt.t =
    let open Lwt_syntax in
    let* v =
      let* v =
        precheck
          ~disable_precheck:false
          ~filter_config
          ~filter_state
          ~validation_state
          op
      in
      match v with
      | `Fail errs ->
          (* Precheck rejected the operation *)
          Lwt.return_error errs
      | `Passed_precheck (filter_state, _new_op, replacement) ->
          (* TODO: handle new_op once parsing cache has been added, needs
             to be replaced with new version that has increased counter *)
          (* Precheck succeeded *)
          let (classification, to_reclassify) =
            match replacement with
            | `No_replace -> (`Prechecked, None)
            | `Replace (old_oph, replacement_classification) ->
                (* Precheck succeeded, but an old operation is replaced *)
                (`Prechecked, Some (old_oph, replacement_classification))
          in
          Lwt.return_ok
            (filter_state, validation_state, classification, to_reclassify)
      | `Undecided -> (
          let* v = Prevalidation_t.apply_operation validation_state op in
          match v with
          | Applied (new_validation_state, receipt) -> (
              (* Apply succeeded, call post_filter *)
              let* v =
                post_filter
                  ~filter_config
                  ~filter_state
                  ~validation_state_before:
                    (Prevalidation_t.validation_state validation_state)
                  ~validation_state_after:
                    (Prevalidation_t.validation_state new_validation_state)
                  op.protocol
                  receipt
              in
              match v with
              | `Passed_postfilter new_filter_state ->
                  (* Post_filter ok, accept operation *)
                  Lwt.return_ok
                    (new_filter_state, new_validation_state, `Applied, None)
              | `Refused _ as op_class ->
                  (* Post_filter refused the operation *)
                  Lwt.return_error op_class)
          (* Apply rejected the operation *)
          | Branch_delayed e -> Lwt.return_error (`Branch_delayed e)
          | Branch_refused e -> Lwt.return_error (`Branch_refused e)
          | Refused e -> Lwt.return_error (`Refused e)
          | Outdated e -> Lwt.return_error (`Outdated e))
    in
    match v with
    | Error err_class ->
        Lwt.return (filter_state, validation_state, err_class, None)
    | Ok (f_state, v_state, classification, to_reclassify) ->
        (*let mempool = Mempool.cons_valid op.hash mempool in*)
        Lwt.return (f_state, v_state, classification, to_reclassify)

  (* Extra tezedge stuff *)

  let is_endorsement (op : protocol_operation) =
    Filter.Proto.acceptable_passes
      {shell = op.shell; protocol_data = op.protocol_data}
    = [0]

  type validator_state =
    | Validator_inactive
    | Validator_active of {
        validation_state : Prevalidation_t.t tzresult;
        filter_state : filter_state;
        current_block_hash : Block_hash.t;
        filter_config : filter_config;
      }

  let validator_state = ref Validator_inactive

  let launch ?protocol_data ~chain_id ~predecessor_context ~predecessor_header
      ~predecessor_hash () =
    let open Lwt_result_syntax in
    let*! validation_state =
      Prevalidation_t.create
        ?protocol_data
        ~chain_id
        ~predecessor_context
        ~predecessor_header
        ~predecessor_hash
        ~timestamp:Time.System.(now () |> to_protocol)
        ()
    in
    let* filter_state =
      Filter.Mempool.init
        Filter.Mempool.default_config
        ?validation_state:
          (Option.map
             Prevalidation_t.validation_state
             (Option.of_result validation_state))
        ~predecessor:predecessor_header
        ()
    in
    validator_state :=
      Validator_active
        {
          validation_state;
          filter_state;
          current_block_hash = predecessor_hash;
          filter_config = Filter.Mempool.default_config;
        } ;
    (* TODO: why was this used in the old implementation? *)
    (* value was used in some tests on the rust side *)
    (*context_fitness validation_state *)
    return_unit

  let validate_operation op =
    let open Lwt_result_syntax in
    match !validator_state with
    | Validator_inactive -> failwith "Validator inactive"
    | Validator_active
        {validation_state; filter_state; current_block_hash; filter_config} -> (
        match validation_state with
        | Error err ->
            failwith
              "Unexpected errored validation state: %a"
              Error_monad.pp_print_trace
              err
        | Ok validation_state ->
            let*! (filter_state, validation_state, classification, to_reclassify)
                =
              classify_operation
                filter_config
                ~filter_state
                ~validation_state
                op
            in
            validator_state :=
              Validator_active
                {
                  filter_state;
                  validation_state = Ok validation_state;
                  current_block_hash;
                  filter_config;
                } ;
            return (classification, is_endorsement op.protocol, to_reclassify))

  let pre_filter_operation op =
    let open Lwt_result_syntax in
    match !validator_state with
    | Validator_inactive -> failwith "Validator inactive"
    | Validator_active {validation_state; filter_state; filter_config; _} ->
        let*! result =
          pre_filter ~filter_config ~filter_state ~validation_state op
        in
        return result

  let operation_data_json (op : protocol_operation operation) =
    let open Result_syntax in
    try
      return
        Data_encoding.Json.(
          to_string ~newline:false ~minify:true
          @@ construct
               Filter.Proto.operation_data_encoding
               op.protocol.protocol_data)
    with exn -> error_with_exn exn
end

module ChainProto_registry = Map.Make (struct
  type t = Chain_id.t * Protocol_hash.t

  let compare (c1, p1) (c2, p2) =
    let pc = Protocol_hash.compare p1 p2 in
    if pc = 0 then Chain_id.compare c1 c2 else pc
end)

let chain_proto_registry : t ChainProto_registry.t ref =
  ref ChainProto_registry.empty

let create (module Filter : Prevalidator_filters.FILTER) chain_id =
  let open Lwt_syntax in
  let limits = default_limits in
  match
    ChainProto_registry.find (chain_id, Filter.Proto.hash) !chain_proto_registry
  with
  | None ->
      let module Prevalidation_t = Prevalidation.Make (Filter.Proto) in
      let module Prevalidator =
        Make
          (Filter)
          (struct
            let limits = limits

            let chain_id = chain_id
          end)
          (Prevalidation_t)
      in
      (* Checking initialization errors before giving a reference to dangerous
       * `worker` value to caller. *)
      (*Prevalidator.initialization_errors >>=? fun () ->*)
      chain_proto_registry :=
        ChainProto_registry.add
          Prevalidator.name
          (module Prevalidator : T)
          !chain_proto_registry ;
      return (module Prevalidator : T)
  | Some p -> return p
