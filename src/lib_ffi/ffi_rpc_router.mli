(*
    (protocol rpc's)
    Calls protocol rpcs
*)
val call_protocol_rpc :
  Ffi_rpc_structs.protocol_rpc_request ->
  (Ffi_rpc_structs.rpc_response, Ffi_rpc_structs.rpc_response_error) result
  Lwt.t
