open Ffi_errors
open Tezos_base__TzPervasives
open Ffi_rpc_structs

(* checkouts context *)
let checkout_context (context_hash : Context_hash.t) =
  let open Lwt_result_syntax in
  let storage = Storage_facade.global_storage in
  let* context_index = Storage_facade.context_index storage in
  let*! context = Context.checkout context_index context_hash in
  return context

let get_protocol_for_hash protocol_hash_from_context =
  let open Lwt_result_syntax in
  match Registered_protocol.get protocol_hash_from_context with
  | None ->
      tzfail
        (Unavailable_protocol
           (Protocol_hash.to_b58check protocol_hash_from_context))
  | Some proto -> return proto

(* copy of Block_services.Make *)
(* TODO: TE-36 - expose stuff in tezos-ocaml shell for not to copy our own? *)
module Shell_block_services
    (Next_proto : Tezos_shell_services.Block_services.PROTO) =
struct
  let next_protocol_hash = Protocol_hash.to_b58check Next_proto.hash

  let next_operation_encoding =
    let open Data_encoding in
    def "next_operation"
    @@ conv
         (fun Next_proto.{shell; protocol_data} -> ((), (shell, protocol_data)))
         (fun ((), (shell, protocol_data)) -> {shell; protocol_data})
         (merge_objs
            (obj1 (req "protocol" (constant next_protocol_hash)))
            (merge_objs
               (dynamic_size Operation.shell_header_encoding)
               (dynamic_size Next_proto.operation_data_encoding)))

  let block_result_encoding =
    let open Data_encoding in
    obj2
      (req "shell_header" Block_header.shell_header_encoding)
      (req "operations" (list (Preapply_result.encoding RPC_error.encoding)))

  type block_param = {
    protocol_data : Next_proto.block_header_data;
    operations : Next_proto.operation list list;
  }

  let block_param_encoding =
    let open Data_encoding in
    conv
      (fun {protocol_data; operations} -> (protocol_data, operations))
      (fun (protocol_data, operations) -> {protocol_data; operations})
      (obj2
         (req
            "protocol_data"
            (conv
               (fun h -> ((), h))
               (fun ((), h) -> h)
               (merge_objs
                  (obj1 (req "protocol" (constant next_protocol_hash)))
                  (dynamic_size Next_proto.block_header_data_encoding))))
         (req "operations" (list (dynamic_size (list next_operation_encoding)))))

  let block_query =
    let open RPC_query in
    query (fun sort timestamp ->
        object
          method sort_operations = sort

          method timestamp = timestamp
        end)
    |+ flag "sort" (fun t -> t#sort_operations)
    |+ opt_field "timestamp" Time.Protocol.rpc_arg (fun t -> t#timestamp)
    |> seal
end

let checkout_context_or_fail context_hash =
  let open Lwt_result_syntax in
  let* context = checkout_context context_hash in
  match context with
  | None -> tzfail @@ Unknown_context (Context_hash.to_b58check context_hash)
  | Some context -> return context

(* this CAN be done, throught protocol api - updater.mli  *)
let call_preapply_operations chain_id block_header request =
  let open Lwt_result_syntax in
  Ffi_logger.ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[RPC - call_preapply_operations] request[%s]: %s"
        request.context_path
        request.body) ;
  (* get context *)
  let* context =
    checkout_context_or_fail block_header.Block_header.shell.context
  in
  (* get protocol from context *)
  let*! result =
    let*! protocol_hash_from_context = Context.get_protocol context in
    (* protocol instance *)
    let* (module Next_proto) =
      get_protocol_for_hash protocol_hash_from_context
    in
    (* Block_services instance *)
    let module Shell_block_services = Shell_block_services (Next_proto) in
    (* input encoding *)
    (* decode request as rpc json *)
    let input_encoding =
      Data_encoding.list Shell_block_services.next_operation_encoding
    in
    let json = (Ezjsonm.from_string request.body :> Data_encoding.json) in
    let ops = Ffi_utils.decode_from_json input_encoding json in
    (* output encoding *)
    let output_encoding =
      Data_encoding.list Next_proto.operation_data_and_receipt_encoding
    in
    (* preapply like Block_directory.Preapply.operations *)
    let predecessor = Block_header.hash block_header in
    let header = block_header.shell in
    let predecessor_context = Shell_context.wrap_disk_context context in
    let* state =
      Next_proto.begin_construction
        ~chain_id
        ~predecessor_context
        ~predecessor_timestamp:header.timestamp
        ~predecessor_level:header.level
        ~predecessor_fitness:header.fitness
        ~predecessor
        ~timestamp:Time.System.(now () |> to_protocol)
        ~cache:`Lazy
        ()
    in
    let* (state, acc) =
      List.fold_left_es
        (fun (state, acc) op ->
          let* (state, result) = Next_proto.apply_operation state op in
          return (state, (op.protocol_data, result) :: acc))
        (state, [])
        ops
    in
    let* _ = Next_proto.finalize_block state None in
    let result = List.rev acc in
    return
      (Ffi_utils.to_json ~newline:false ~minify:true output_encoding result)
  in
  match result with
  | Ok json ->
      Ffi_logger.ffi_log ~msg:(fun () ->
          Printf.sprintf
            "[RPC - call_preapply_operations] response[%s]: %s"
            request.context_path
            json) ;
      return {body = json}
  | Error error ->
      Format.kasprintf Ffi_errors.failwith "%a" pp_print_trace error

(* this CAN be done, throught protocol api - updater.mli  *)
let call_preapply_block chain_id block_header request
    (predecessor_block_metadata_hash : Block_metadata_hash.t option)
    (predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option)
    predecessor_max_operations_ttl =
  let open Lwt_result_syntax in
  Ffi_logger.ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[RPC - call_preapply_block] request[%s]: %s"
        request.context_path
        request.body) ;
  (* get context *)
  let* context =
    checkout_context_or_fail block_header.Block_header.shell.context
  in
  (* get protocol from context *)
  let*! protocol_hash_from_context = Context.get_protocol context in
  (* protocol instance *)
  let* (module Next_proto) = get_protocol_for_hash protocol_hash_from_context in
  (* Block_services instance *)
  let module Shell_block_services = Shell_block_services (Next_proto) in
  (* input encoding *)
  (* decode request as rpc json *)
  let input_encoding = Shell_block_services.block_param_encoding in
  let json = Ezjsonm.from_string request.body in
  let p = Ffi_utils.decode_from_json input_encoding json in
  (* output encoding *)
  let output_encoding = Shell_block_services.block_result_encoding in
  (* parse query params *)
  let uri = Uri.of_string request.context_path in
  let q = List.map (fun (n, vs) -> (n, String.concat "," vs)) (Uri.query uri) in
  let q = RPC_query.parse Shell_block_services.block_query q in
  (* preapply like Block_directory.Preapply.operations - register0 S.Helpers.Preapply.block *)
  let timestamp =
    match q#timestamp with
    | None -> Time.System.(now () |> to_protocol)
    | Some time -> time
  in
  let protocol_data =
    Data_encoding.Binary.to_bytes_exn
      Next_proto.block_header_data_encoding
      p.protocol_data
  in
  let operations =
    List.map
      (fun operations ->
        let operations =
          if q#sort_operations then
            List.sort Next_proto.relative_position_within_block operations
          else operations
        in
        List.map
          (fun op ->
            let proto =
              Data_encoding.Binary.to_bytes_exn
                Next_proto.operation_data_encoding
                op.Next_proto.protocol_data
            in
            {Operation.shell = op.shell; proto})
          operations)
      p.operations
  in
  let predecessor_shell_header = block_header.shell in
  let predecessor_hash = Block_header.hash block_header in
  (* TODO: could we make use of these? *)
  let live_operations = Operation_hash.Set.empty in
  let live_blocks = Block_hash.Set.empty in
  let* (result, _apply_result_and_context) =
    Ffi_block_validation.preapply
      ~chain_id
      ~user_activated_upgrades:!Ffi_block_validation.forced_protocol_upgrades
      ~user_activated_protocol_overrides:
        !Ffi_block_validation.voted_protocol_overrides
      ~operation_metadata_size_limit:None
      ~timestamp
      ~protocol_data
      ~live_blocks
      ~live_operations
      ~predecessor_context:context
      ~predecessor_shell_header
      ~predecessor_hash
      ~predecessor_max_operations_ttl
      ~predecessor_block_metadata_hash
      ~predecessor_ops_metadata_hash
      operations
    (* TODO: what to do with apply_result_and_context? *)
  in
  let json =
    Ffi_utils.to_json ~newline:false ~minify:true output_encoding result
  in
  Ffi_logger.ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[RPC - call_preapply_block] response[%s]: %s"
        request.context_path
        json) ;
  return {body = json}
