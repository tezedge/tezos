open Lwt.Syntax

module IpcMessage = struct
  type rw_message_t = [`ApplyBlockCall of Apply_block.apply_block_request]

  type compute_path_request = {operations : Operation_hash.t list list}

  type dump_context_request = {
    context_hash : Context_hash.t;
    dump_into_path : string;
  }

  type restore_context_request = {
    expected_context_hash : Context_hash.t;
    restore_from_path : string;
    nb_context_elements : int;
  }

  type integrity_check_context_request = {
    context_path : string;
    auto_repair : bool;
  }

  type ro_message_t =
    [ `AssertEncodingForProtocolDataCall of Protocol_hash.t * bytes
    | (* TODO: BeginApplicationCall can be removed *)
      `BeginApplicationCall of
      Validate_block.begin_application_request
    | `BeginConstruction of Validate_block.begin_construction_request
    | `PreFilterOperation of Validate_block.prevalidator_request
    | `ValidateOperation of Validate_block.prevalidator_request
    | `PreapplyBlock of Preapply_block.preapply_block_request
    | `ProtocolRpcCall of Ffi_rpc_structs.protocol_rpc_request
    | `HelpersPreapplyOperationsCall of Ffi_rpc_structs.protocol_rpc_request
    | `HelpersPreapplyBlockCall of
      Ffi_rpc_structs.helpers_preapply_block_request
    | `ComputePathCall of compute_path_request
    | `GenesisResultDataCall of
      Block_explorer.commit_genesis_result_data_request
    | `JsonEncodeApplyBlockResultMetadata of
      Ffi.json_encode_apply_block_result_metadata_request
    | `JsonEncodeApplyBlockOperationsMetadata of
      Ffi.json_encode_apply_block_operations_metadata_request ]

  (* TODO: remove these 3 once not needed anymore *)
  (*type ro_context_t =
    [ `ContextGetKeyFromHistory of unit
    | `ContextGetKeyValuesByPrefix of unit
    | `ContextGetTreeByPrefix of unit ]*)

  type ro_context_t = [`ContextGetLatestContextHashes of int]

  type control_message_t =
    [ `ChangeRuntimeConfigurationCall of Ffi_config.RuntimeConfiguration.t
    | `InitProtocolContextCall of Ffi_config.Context.t
    | `InitProtocolContextIpcServer of string
    | `DumpContext of dump_context_request
    | `RestoreContext of restore_context_request
    | `IntegrityCheckContext of integrity_check_context_request
    | `Ping
    | `ShutdownCall ]

  type t = [rw_message_t | ro_message_t | ro_context_t | control_message_t]

  external ffi_decode_message : bytes -> (t, string) result
    = "call_tezedge_ipc_decode_request"

  let decode buf = ffi_decode_message buf
end

module IpcResponse = struct
  type 'result ipc_result = ('result, Ffi_error_wrapper.error_trace) result

  type compute_path_response = {
    operations_hashes_path : Operation_list_list_hash.path list;
  }

  type t =
    [ `ApplyBlockResult of Apply_block.apply_block_response ipc_result
    | `AssertEncodingForProtocolDataResult of unit ipc_result
    | `BeginApplicationResult of
      Validate_block.begin_application_response ipc_result
    | `BeginConstructionResult of Validate_block.prevalidator_wrapper ipc_result
    | `ValidateOperationResponse of
      Validate_block.validate_operation_response ipc_result
    | `PreFilterOperationResult of
      Validate_block.pre_filter_operation_response ipc_result
    | `PreapplyBlockResponse of
      Preapply_block.preapply_block_response ipc_result
    | `RpcResponse of
      (Ffi_rpc_structs.rpc_response, Ffi_rpc_structs.rpc_response_error) result
    | `HelpersPreapplyResponse of
      Ffi_rpc_structs.helpers_preapply_response ipc_result
    | `ChangeRuntimeConfigurationResult
    | `InitProtocolContextResult of Ffi.init_protocol_context_result ipc_result
    | `InitProtocolContextIpcServerResult of (unit, string) result
    | `CommitGenesisResultData of
      Block_explorer.commit_genesis_result_data ipc_result
    | `ComputePathResponse of compute_path_response ipc_result
    | `JsonEncodeApplyBlockResultMetadataResponse of string ipc_result
    | `JsonEncodeApplyBlockOperationsMetadata of string ipc_result
    | `IpcResponseEncodingFailure of string
    | `ContextGetLatestContextHashesResult of Context_hash.t list ipc_result
    | `DumpContextResponse of int ipc_result
    | `RestoreContextResponse of unit ipc_result
    | `IntegrityCheckContextResponse of unit ipc_result
    | `PingResult
    | `ShutdownResult ]

  external ffi_encode_response : t -> (bytes, string) result
    = "call_tezedge_ipc_encode_response"

  let encode t = ffi_encode_response t
end

let encoding_failure msg =
  match IpcResponse.encode (`IpcResponseEncodingFailure msg) with
  | Ok bytes -> bytes
  | Error msg ->
      Logs.err (fun m ->
          m
            "FATAL: Unexpected error when trying to encode \
             IpcResponseEncodingFailure '%s'"
            msg) ;
      assert false

let message_name (msg : IpcMessage.t) =
  match msg with
  | `ApplyBlockCall _ -> "ApplyBlockCall"
  | `AssertEncodingForProtocolDataCall _ -> "AssertEncodingForProtocolDataCall"
  | `BeginApplicationCall _ -> "BeginApplicationCall"
  | `BeginConstruction _ -> "BeginConstruction"
  | `PreFilterOperation _ -> "PreFilterOperation"
  | `ValidateOperation _ -> "ValidateOperation"
  | `PreapplyBlock _ -> "PreapplyBlock"
  | `ProtocolRpcCall _ -> "ProtocolRpcCall"
  | `HelpersPreapplyOperationsCall _ -> "HelpersPreapplyOperationsCall"
  | `HelpersPreapplyBlockCall _ -> "HelpersPreapplyBlockCall"
  | `ComputePathCall _ -> "ComputePathCall"
  | `ChangeRuntimeConfigurationCall _ -> "ChangeRuntimeConfigurationCall"
  | `InitProtocolContextCall _ -> "InitProtocolContextCall"
  | `InitProtocolContextIpcServer _ -> "InitProtocolContextIpcServer"
  | `GenesisResultDataCall _ -> "GenesisResultDataCall"
  | `JsonEncodeApplyBlockResultMetadata _ ->
      "JsonEncodeApplyBlockResultMetadata"
  | `JsonEncodeApplyBlockOperationsMetadata _ ->
      "JsonEncodeApplyBlockOperationsMetadata"
  | `ContextGetLatestContextHashes _ -> "ContextGetLatestContextHashes"
  | `DumpContext _ -> "DumpContext"
  | `RestoreContext _ -> "RestoreContext"
  | `IntegrityCheckContext _ -> "IntegrityCheckContext"
  | `Ping -> "Ping"
  | `ShutdownCall -> "ShutdownCall"

let to_tezedge_index index =
  let open Lwt_result_syntax in
  match Context.get_tezedge_index index with
  | Some index -> return index
  | None -> failwith "The supplied index doesn't contain a TezEdge index"

let tezedge_index_latest_context_hashes count =
  let open Lwt_result_syntax in
  let storage = Storage_facade.global_storage in
  let* context_index = Storage_facade.context_index storage in
  let* tezedge_index = to_tezedge_index context_index in
  let*! result = Tezedge_context.latest_context_hashes tezedge_index count in
  match result with
  | Ok hashes -> return hashes
  | Error error -> failwith "%s" error

let dump_irmin_context context_index context_hash path =
  let* fd = Lwt_unix.openfile path Unix.[O_CREAT; O_TRUNC; O_WRONLY] 0o444 in
  Lwt.finalize
    (fun () ->
      Context.dump_context
        context_index
        context_hash
        ~on_disk:true
        ~progress_display_mode:Never
        ~fd)
    (fun () -> Lwt_unix.close fd)

module Tezedge_dump = struct
  external dump : string -> Context_hash.t -> string -> int
    = "call_tezedge_context_dump"

  external restore : string -> string -> Context_hash.t -> int -> unit
    = "call_tezedge_context_restore"
end

let dump_tezedge_context context_path context_hash target_path =
  let open Lwt_result_syntax in
  let n = Tezedge_dump.dump context_path context_hash target_path in
  return n

let dump_context context_hash path =
  let open Lwt_result_syntax in
  let open Ffi_config.Context in
  let storage = Storage_facade.global_storage in
  let* config = Storage_facade.config storage in
  let* context_index = Storage_facade.context_index storage in
  match config.storage with
  | Irmin_only _ -> dump_irmin_context context_index context_hash path
  | Tezedge_only {backend = OnDisk {base_path; _}; _} ->
      dump_tezedge_context base_path context_hash path
  | Tezedge_only _ ->
      tzfail
        (Ffi_errors.Ffi_call_error
           "Can only perform dump of on-disk backend for TezEdge context")
  | Both _ ->
      tzfail
        (Ffi_errors.Ffi_call_error
           "Cannot perform a context dump of both storages at the same time")

let restore_irmin_context context_index context_file_path ~expected_context_hash
    ~nb_context_elements =
  let open Lwt_result_syntax in
  let* fd =
    Lwt.catch
      (fun () ->
        let*! fd =
          Lwt_unix.openfile context_file_path Lwt_unix.[O_RDONLY] 0o444
        in
        return fd)
      (function
        | Unix.Unix_error (e, _, _) ->
            tzfail (Context.Cannot_open_file (Unix.error_message e))
        | exc ->
            let msg =
              Printf.sprintf "unknown error: %s" (Printexc.to_string exc)
            in
            tzfail (Context.Cannot_open_file msg))
  in
  Lwt.finalize
    (fun () ->
      Context.restore_context
        context_index
        ~expected_context_hash
        ~fd
        ~legacy:false
        ~in_memory:false
        ~progress_display_mode:Never
        ~nb_context_elements)
    (fun () -> Lwt_unix.close fd)

let restore_tezedge_context restore_path _context_index context_file_path
    ~expected_context_hash ~nb_context_elements =
  let open Lwt_result_syntax in
  let result =
    Tezedge_dump.restore
      restore_path
      context_file_path
      expected_context_hash
      nb_context_elements
  in
  return result

let restore_context context_file_path ~expected_context_hash
    ~nb_context_elements =
  let open Lwt_result_syntax in
  let storage = Storage_facade.global_storage in
  let* config = Storage_facade.config storage in
  let* context_index = Storage_facade.context_index storage in
  match config.storage with
  | Irmin_only _ ->
      restore_irmin_context
        context_index
        context_file_path
        ~expected_context_hash
        ~nb_context_elements
  | Tezedge_only {backend = OnDisk {base_path; _}; _} ->
      restore_tezedge_context
        (String.concat "/" [base_path; "context"])
        context_index
        context_file_path
        ~expected_context_hash
        ~nb_context_elements
  | Tezedge_only _ ->
      tzfail
        (Ffi_errors.Ffi_call_error
           "Can only perform restore of on-disk backend for TezEdge context")
  | Both _ ->
      tzfail
        (Ffi_errors.Ffi_call_error
           "Cannot perform a context restore of both storages at the same time")

let process_message (msg : IpcMessage.t) resolve shutdown =
  (* TODO: needs to be able to handle streaming too *)
  let resolve r =
    match IpcResponse.encode r with
    | Ok bytes -> resolve bytes
    | Error msg -> resolve (encoding_failure msg)
  in
  let protected f =
    Ffi_error_wrapper.(
      let+ trace = lwt_exception_protect f in
      postprocess_trace trace)
  in
  match msg with
  | `ApplyBlockCall req ->
      let* result = protected (fun () -> Ffi.apply_block req) in
      resolve (`ApplyBlockResult result)
  | `AssertEncodingForProtocolDataCall (protocol_hash, data) ->
      let result = Ffi.assert_encoding_for_protocol_data protocol_hash data in
      let result = Ffi_error_wrapper.postprocess_trace result in
      resolve (`AssertEncodingForProtocolDataResult result)
  | `BeginApplicationCall req ->
      let* result = protected (fun () -> Ffi.begin_application req) in
      resolve (`BeginApplicationResult result)
  | `BeginConstruction req ->
      let* result = protected (fun () -> Ffi.begin_construction req) in
      resolve (`BeginConstructionResult result)
  | `PreFilterOperation req ->
      let* result = protected (fun () -> Ffi.pre_filter_operation req) in
      resolve (`PreFilterOperationResult result)
  | `ValidateOperation req ->
      let* result = protected (fun () -> Ffi.validate_operation req) in
      resolve (`ValidateOperationResponse result)
  | `PreapplyBlock req ->
      let* result = protected (fun () -> Preapply_block.preapply_block req) in
      resolve (`PreapplyBlockResponse result)
  | `ProtocolRpcCall req ->
      let* result = Ffi_rpc_router.call_protocol_rpc req in
      resolve (`RpcResponse result)
  | `HelpersPreapplyOperationsCall req ->
      let* result = protected (fun () -> Ffi.helpers_preapply_operations req) in
      resolve (`HelpersPreapplyResponse result)
  | `HelpersPreapplyBlockCall req ->
      let* result = protected (fun () -> Ffi.helpers_preapply_block req) in
      resolve (`HelpersPreapplyResponse result)
  | `ComputePathCall req ->
      let result = Ffi.compute_path req.operations in
      let result =
        Result.map
          (fun operations_hashes_path -> IpcResponse.{operations_hashes_path})
          result
      in
      let result = Ffi_error_wrapper.postprocess_trace result in
      resolve (`ComputePathResponse result)
  | `ChangeRuntimeConfigurationCall cfg ->
      Ffi.change_runtime_configuration cfg ;
      resolve `ChangeRuntimeConfigurationResult
  | `InitProtocolContextCall cfg ->
      let* result = protected (fun () -> Ffi.init_protocol_context cfg) in
      resolve (`InitProtocolContextResult result)
  | `InitProtocolContextIpcServer _ ->
      (* TODO: needs to call a rust function to initialize it or be reimplemented in OCaml *)
      resolve (`InitProtocolContextIpcServerResult (Ok ()))
  | `GenesisResultDataCall req ->
      let* result = protected (fun () -> Ffi.genesis_result_data req) in
      resolve (`CommitGenesisResultData result)
  | `JsonEncodeApplyBlockResultMetadata req ->
      let* result = protected (fun () -> Ffi.apply_block_result_metadata req) in
      resolve (`JsonEncodeApplyBlockResultMetadataResponse result)
  | `JsonEncodeApplyBlockOperationsMetadata req ->
      let* result =
        protected (fun () -> Ffi.apply_block_operations_metadata req)
      in
      resolve (`JsonEncodeApplyBlockOperationsMetadata result)
  | `ContextGetLatestContextHashes count ->
      let* result =
        protected (fun () -> tezedge_index_latest_context_hashes count)
      in
      resolve (`ContextGetLatestContextHashesResult result)
  | `DumpContext req ->
      let IpcMessage.{context_hash; dump_into_path} = req in
      let* result =
        protected (fun () -> dump_context context_hash dump_into_path)
      in
      resolve (`DumpContextResponse result)
  | `RestoreContext req ->
      let IpcMessage.
            {expected_context_hash; nb_context_elements; restore_from_path} =
        req
      in
      let* result =
        protected (fun () ->
            restore_context
              restore_from_path
              ~expected_context_hash
              ~nb_context_elements)
      in
      resolve (`RestoreContextResponse result)
  | `IntegrityCheckContext req ->
      let IpcMessage.{context_path; auto_repair} = req in
      let* result =
        protected (fun () ->
            let* () =
              Context.Checks.Pack.Integrity_check.run
                ~root:context_path
                ~auto_repair
            in
            Lwt.return_ok ())
      in
      resolve (`IntegrityCheckContextResponse result)
  | `Ping -> resolve `PingResult
  | `ShutdownCall ->
      shutdown := true ;
      resolve `ShutdownResult

let ensure_buf_capacity buf_ref size =
  if Bytes.length !buf_ref < size then buf_ref := Bytes.create size

(* Get and decode the next message from the stream. Returns None on EOF. *)
let get_next_message buf_ref ic =
  Lwt.catch
    (fun () ->
      let* msg_length = Lwt_io.BE.read_int ic in
      ensure_buf_capacity buf_ref msg_length ;
      let buf = !buf_ref in
      let* () = Lwt_io.read_into_exactly ic buf 0 msg_length in
      let decoded = IpcMessage.decode buf in
      Lwt.return (Some decoded))
    (function End_of_file -> Lwt.return None | exn -> Lwt.fail exn)

(* Loop that reads and processes messages from a connection one by one *)
let rec handle_connection ic oc shutdown processing_message_name () =
  let buf_ref = ref (Bytes.create 8192) in
  processing_message_name := "Decoding" ;
  let* msg = get_next_message buf_ref ic in
  match msg with
  | Some (Ok msg) ->
      let respond answer_bytes =
        let msg_len = Bytes.length answer_bytes in
        let* () = Lwt_io.BE.write_int oc msg_len in
        let* () = Lwt_io.write_from_exactly oc answer_bytes 0 msg_len in
        Lwt.return ()
      in
      processing_message_name := message_name msg ;
      let* () = process_message msg respond shutdown in
      if not !shutdown then
        handle_connection ic oc shutdown processing_message_name ()
      else Lwt.return_unit
  | Some (Error err) ->
      let* () =
        Logs_lwt.warn (fun m ->
            m "Error when decoding message, closing connection: %s" err)
      in
      Lwt.return ()
  | None ->
      let* () = Logs_lwt.debug (fun m -> m "Connection closed") in
      Lwt.return ()

let close_channel ~name c =
  if not (Lwt_io.is_closed c) then
    Lwt.on_failure (Lwt_io.close c) (fun e ->
        Logs.debug (fun m ->
            m
              "Failure when closing channel '%s': %s"
              name
              (Printexc.to_string e)))
  else ()

(* Processes a new connection *)
let process_connection conn shutdown =
  let (fd, _) = conn in
  let ic = Lwt_io.of_fd ~mode:Lwt_io.Input fd in
  let oc = Lwt_io.of_fd ~mode:Lwt_io.Output fd in
  let cleanup () =
    close_channel ~name:"input" ic ;
    close_channel ~name:"output" oc
  in
  let processing_message_name = ref "Unkown" in
  let handle_error e =
    Logs.err (fun m ->
        m
          "Failure in `handle_connection` message-type=%s: %s"
          !processing_message_name
          (Printexc.to_string e) ;
        cleanup ())
  in
  Lwt.on_failure
    (let* () = handle_connection ic oc shutdown processing_message_name () in
     cleanup () ;
     Lwt.return_unit)
    handle_error ;
  let* () = Logs_lwt.debug (fun m -> m "New connection") in
  Lwt.return ()

(* Socket listen call backlog. -1 = default to max specified by system *)
let backlog = -1

let create_socket socket_path =
  let open Lwt_unix in
  let sock = Lwt_unix.socket PF_UNIX SOCK_STREAM 0 in
  let* () = bind sock (ADDR_UNIX socket_path) in
  listen sock backlog ;
  Lwt.return sock

(* Returns a loop that will accept and process new connections *)
let create_server sock shutdown =
  let rec serve () =
    if not !shutdown then
      let* conn = Lwt_unix.accept sock in
      let* () = process_connection conn shutdown in
      serve ()
    else Lwt.return_unit
  in
  serve

let main sock_cmd_path =
  Tezos_tezedge_fuzzing.Coverage.register_dump_callback () ;
  let shutdown = ref false in
  (* TODO: catch non-Exit exceptions *)
  let start () =
    let _ = try Sys.remove sock_cmd_path with _ -> () in
    let* sock = create_socket sock_cmd_path in
    let serve_loop = create_server sock shutdown in
    (* Set the thread name to make it easier to identify under the fuzzing setup *)
    Tezos_tezedge_fuzzing.Coverage.default_thread_name () ;
    serve_loop ()
  in
  let _id =
    Lwt_exit.register_clean_up_callback ~loc:"main" (fun _n ->
        shutdown := true ;
        Lwt.return_unit)
  in
  Ok
    (Lwt_main.run
    @@ let* result = Lwt_exit.wrap_and_error (start ()) in
       match result with
       | Ok () -> Lwt_exit.exit_and_wait 0
       | Error code -> Lwt_exit.exit_and_wait code)

(* For tests *)
let apply_encoded_message buf =
  let result = ref Bytes.empty in
  let handle_result answer_bytes =
    result := answer_bytes ;
    Lwt.return_unit
  in
  match IpcMessage.decode buf with
  | Error err -> Error ("Error decoding message: " ^ err)
  | Ok msg ->
      let shutdown = ref false in
      Lwt_main.run
      @@ let* () = process_message msg handle_result shutdown in
         Lwt.return (Ok !result)
