open Ffi_converter

type bytes = Bytes.t

type hash = Bytes.t

module Bytes_converter = Bytes_bytes_converter
module Hash_converter = Hash_converter

let return_result result =
  Gc.minor () ;
  result

let chain_id_roundtrip (chain_id : hash) =
  return_result @@ Hash_converter.of_chain_id
  @@ Hash_converter.to_chain_id chain_id

let operation_roundtrip (operation : bytes) =
  return_result @@ Bytes_converter.from_mbytes @@ Operation.to_bytes
  @@ Operation.of_bytes_exn
  @@ Bytes_converter.to_mbytes operation

let operations_list_list_roundtrip (operations_list_list : bytes list list) =
  return_result
  @@ operations_list_list_to_bytes ~from_mbytes:Bytes_converter.from_mbytes
  @@ operations_list_list_from_bytes
       ~to_mbytes:Bytes_converter.to_mbytes
       operations_list_list

let block_header_roundtrip (header : bytes) =
  (* to structs *)
  let block_header =
    Block_header.of_bytes_exn @@ Bytes_converter.to_mbytes header
  in
  let block_header_hash = Block_header.hash block_header in
  (* back to bytes *)
  return_result
    ( Hash_converter.of_block_hash block_header_hash,
      Bytes_converter.from_mbytes @@ Block_header.to_bytes block_header )

let block_header_struct_roundtrip (block_header : Block_header.t) =
  (* calculate hash *)
  let block_header_hash = Block_header.hash block_header in
  let chain_id = Chain_id.of_block_hash block_header_hash in
  (* back to ffi *)
  return_result
    ( Hash_converter.of_block_hash block_header_hash,
      Hash_converter.of_chain_id chain_id )

let block_header_with_hash_roundtrip (header_hash : hash) (header : bytes) =
  (* to structs *)
  let block_header_hash = Hash_converter.to_block_hash header_hash in
  let block_header =
    Block_header.of_bytes_exn @@ Bytes_converter.to_mbytes header
  in
  (* back to bytes *)
  return_result
    ( Hash_converter.of_block_hash block_header_hash,
      Bytes_converter.from_mbytes @@ Block_header.to_bytes block_header )

let apply_block_params_roundtrip (chain_id : hash) (block_header : bytes)
    (operations : bytes list list) =
  (* to structs *)
  let chain_id = Hash_converter.to_chain_id chain_id in
  let block_header =
    Block_header.of_bytes_exn @@ Bytes_converter.to_mbytes block_header
  in
  let operations_list_list =
    operations_list_list_from_bytes
      ~to_mbytes:Bytes_converter.to_mbytes
      operations
  in
  (* back to bytes *)
  return_result
    ( Hash_converter.of_chain_id chain_id,
      Bytes_converter.from_mbytes @@ Block_header.to_bytes block_header,
      operations_list_list_to_bytes
        ~from_mbytes:Bytes_converter.from_mbytes
        operations_list_list )

(* TODO: remove from here and the Rust FFI code, not used anymore *)
let context_callback_roundtrip (_count : int) (_context_hash : hash)
    (_header_hash : hash) (_operation_hash : hash) (_key : string list)
    (_data : bytes) =
  return_result ()

(* For benchmarks *)

let dummy_apply_block_response : Apply_block.apply_block_response option ref =
  ref None

let setup_benchmark_apply_block_response
    (response : Apply_block.apply_block_response) =
  dummy_apply_block_response := Some response

let apply_block_request_decoded_roundtrip
    (_request : Apply_block.apply_block_request) =
  let response =
    match !dummy_apply_block_response with
    | Some response -> response
    | None ->
        raise
          (Invalid_argument
             "setup_benchmark_apply_block_response was not called")
  in
  return_result response

(* For value Rust<->OCaml mapping tests *)

let construct_and_compare_hash rust_hash hash_bytes =
  match Blake2B.of_string_opt hash_bytes with
  | None -> false
  | Some ocaml_hash -> Blake2B.(rust_hash = ocaml_hash)

let construct_and_compare_apply_block_request rust_apply_block_request chain_id
    block_header pred_header max_operations_ttl operations
    predecessor_block_metadata_hash predecessor_ops_metadata_hash =
  let apply_block_request =
    Apply_block.
      {
        chain_id;
        block_header;
        pred_header;
        max_operations_ttl;
        operations;
        predecessor_block_metadata_hash;
        predecessor_ops_metadata_hash;
      }
  in
  rust_apply_block_request = apply_block_request

let construct_and_compare_cycle_rolls_owner_snapshot
    rust_cycle_rolls_owner_snapshot cycle seed_bytes rolls_data last_roll =
  let cycle_rolls_owner_snapshot =
    Validation_helpers.{cycle; seed_bytes; rolls_data; last_roll}
  in
  rust_cycle_rolls_owner_snapshot = cycle_rolls_owner_snapshot

let construct_and_compare_apply_block_response rust_apply_block_response
    validation_result_message context_hash protocol_hash next_protocol_hash
    block_header_proto_json block_header_metadata_bytes
    operations_metadata_bytes max_operations_ttl last_allowed_fork_level
    forking_testchain forking_testchain_data block_metadata_hash
    ops_metadata_hashes ops_metadata_hash cycle_rolls_owner_snapshots
    new_protocol_constants_json new_cycle_eras_json commit_time
    execution_timestamps =
  let cycle = None in
  let cycle_position = None in
  let apply_block_response =
    Validation_helpers.
      {
        validation_result_message;
        context_hash;
        protocol_hash;
        next_protocol_hash;
        block_header_proto_json;
        block_header_metadata_bytes;
        operations_metadata_bytes;
        max_operations_ttl;
        last_allowed_fork_level;
        forking_testchain;
        forking_testchain_data;
        block_metadata_hash;
        ops_metadata_hashes;
        ops_metadata_hash;
        cycle;
        cycle_position;
        cycle_rolls_owner_snapshots;
        new_protocol_constants_json;
        new_cycle_eras_json;
        commit_time;
        execution_timestamps;
      }
  in
  rust_apply_block_response = apply_block_response

let construct_and_compare_block_header rust_block_header level proto_level
    validation_passes timestamp predecessor operations_hash fitness context
    protocol_data =
  let shell =
    Block_header.
      {
        level;
        proto_level;
        predecessor;
        timestamp;
        validation_passes;
        operations_hash;
        fitness;
        context;
      }
  in
  let block_header = Block_header.{shell; protocol_data} in
  rust_block_header = block_header

let construct_and_compare_begin_construction_request
    rust_begin_construction_request chain_id predecessor predecessor_hash
    protocol_data =
  let begin_construction_request =
    Validate_block.{chain_id; predecessor; predecessor_hash; protocol_data}
  in
  rust_begin_construction_request = begin_construction_request

let construct_and_compare_validate_operation_request
    rust_validate_operation_request prevalidator operation_hash operation =
  let validate_operation_request =
    Validate_block.
      {
        prevalidator;
        operation_hash;
        operation;
        include_operation_data_json = true;
      }
  in
  rust_validate_operation_request = validate_operation_request

let construct_and_compare_rpc_request rust_rpc_request body context_path meth
    content_type accept =
  let rpc_request =
    Ffi_rpc_structs.{body; context_path; meth; content_type; accept}
  in
  rust_rpc_request = rpc_request

let construct_and_compare_protocol_rpc_request rust_protocol_rpc_request
    block_header chain_id chain_arg request =
  let protocol_rpc_request =
    Ffi_rpc_structs.{block_header; chain_id; chain_arg; request}
  in
  rust_protocol_rpc_request = protocol_rpc_request

let construct_and_compare_operation rust_operation branch proto =
  let shell = Operation.{branch} in
  let operation = Operation.{shell; proto} in
  rust_operation = operation

let construct_and_compare_prevalidator_wrapper rust_prevalidator_wrapper
    chain_id protocol predecessor =
  let prevalidator_wrapper = Validate_block.{chain_id; protocol; predecessor} in
  rust_prevalidator_wrapper = prevalidator_wrapper
