open Ffi_errors
open Tezos_base__TzPervasives

let mbytes_to_hex_string (bytes : Bytes.t) = Hex.show @@ Hex.of_bytes bytes

let hex_string_to_mbytes hex_string = Hex.to_bytes (`Hex hex_string)

let decode_from_mbytes encoding (data : Bytes.t) =
  Data_encoding.Binary.of_bytes_exn encoding data

let decode_from_json encoding (data : Data_encoding.json) =
  Data_encoding.Json.destruct encoding data

let encode_to_mbytes encoding data =
  Data_encoding.Binary.to_bytes_exn encoding data

let to_json ?(newline = false) ?(minify = false) encoding data =
  Data_encoding.Json.to_string
    ~newline
    ~minify
    (Data_encoding.Json.construct encoding data)

let to_hex encoding data =
  mbytes_to_hex_string @@ Data_encoding.Binary.to_bytes_exn encoding data

let ensure_dir_exists dir =
  let open Lwt_result_syntax in
  match Sys.file_exists dir with
  | true -> (
      match Sys.is_directory dir with
      | true -> return_unit
      | false ->
          tzfail
            (Ffi_call_error (Printf.sprintf "dir '%s' is not a directory!" dir))
      )
  | false ->
      tzfail (Ffi_call_error (Printf.sprintf "dir '%s' does not exists!" dir))

let compute_operation_path hashes =
  let path_list =
    Operation_list_list_hash.compute_path
      (List.map Operation_list_hash.compute hashes)
  in
  List.mapi (fun i __ -> path_list i) hashes
