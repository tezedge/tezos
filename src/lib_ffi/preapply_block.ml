open Tezos_base__TzPervasives
open Ffi_errors

type preapply_block_request = {
  chain_id : Chain_id.t;
  protocol_data : Bytes.t;
  timestamp : int64 option;
  operations : Operation.t list list;
  predecessor_header : Block_header.t;
  predecessor_block_metadata_hash : Block_metadata_hash.t option;
  predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option;
  predecessor_max_operations_ttl : int;
}

type preapply_block_response = {
  level : Int32.t;
  proto_level : int;
  predecessor : Block_hash.t;
  timestamp : Time.Protocol.t;
  validation_passes : int;
  operations_hash : Operation_list_list_hash.t;
  fitness : Fitness.t;
  context : Context_hash.t;
  applied_operations : Operation_hash.t list;
}

let checkout_context (context_hash : Context_hash.t) =
  let open Lwt_result_syntax in
  let storage = Storage_facade.global_storage in
  let* context_index = Storage_facade.context_index storage in
  let*! context = Context.checkout context_index context_hash in
  return context

let checkout_context_or_fail context_hash =
  let open Lwt_result_syntax in
  let* context = checkout_context context_hash in
  match context with
  | None -> tzfail @@ Unknown_context (Context_hash.to_b58check context_hash)
  | Some context -> return context

let get_applied_operation_hashes acc (preapply_result : error Preapply_result.t)
    =
  List.append acc (List.map fst preapply_result.applied)

let preapply_block (req : preapply_block_request) =
  let open Lwt_result_syntax in
  let {
    chain_id;
    protocol_data;
    timestamp;
    operations;
    predecessor_header;
    predecessor_block_metadata_hash;
    predecessor_ops_metadata_hash;
    predecessor_max_operations_ttl;
  } =
    req
  in
  let* context = checkout_context_or_fail predecessor_header.shell.context in
  let predecessor_shell_header = predecessor_header.shell in
  let predecessor_hash = Block_header.hash predecessor_header in
  (* TODO: could we make use of these? *)
  let live_operations = Operation_hash.Set.empty in
  let live_blocks = Block_hash.Set.empty in
  let timestamp = Option.map Time.Protocol.of_seconds timestamp in
  let timestamp =
    Option.value_f timestamp ~default:(fun () ->
        Time.System.(now () |> to_protocol))
  in
  let* (result, _apply_result_and_context) =
    Ffi_block_validation.preapply
      ~chain_id
      ~user_activated_upgrades:!Ffi_block_validation.forced_protocol_upgrades
      ~user_activated_protocol_overrides:
        !Ffi_block_validation.voted_protocol_overrides
      ~operation_metadata_size_limit:None
      ~timestamp
      ~protocol_data
      ~live_blocks
      ~live_operations
      ~predecessor_context:context
      ~predecessor_shell_header
      ~predecessor_hash
      ~predecessor_max_operations_ttl
      ~predecessor_block_metadata_hash
      ~predecessor_ops_metadata_hash
      operations
  in
  let (header, operation_results) = result in
  let applied_operations =
    List.fold_left get_applied_operation_hashes [] operation_results
  in
  let {
    Block_header.level;
    proto_level;
    predecessor;
    timestamp;
    validation_passes;
    operations_hash;
    fitness;
    context;
  } =
    header
  in
  return
    {
      level;
      proto_level;
      predecessor;
      timestamp;
      validation_passes;
      operations_hash;
      fitness;
      context;
      applied_operations;
    }
