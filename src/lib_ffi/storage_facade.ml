open Ffi_errors
open Ffi_utils

type storage_t = {
  mutable distributed_db : Distributed_db.t option;
  mutable context_index : Context.index option;
  mutable enable_testchain : bool;
  mutable config : Ffi_config.Context.t option;
}

let default_enable_testchain = false

let global_storage : storage_t =
  {
    distributed_db = None;
    context_index = None;
    enable_testchain = default_enable_testchain;
    config = None;
  }

(* ==== UNUSED CODE ====
 *
 * let global_state storage =
 *   match storage.global_state with
 *   | Some storage ->
 *       return storage
 *   | None ->
 *       fail (Ffi_call_error "No 'global_state' is present!")
 *
 * let chain (storage : storage_t) (chain_id : Chain_id.t) =
 *   global_state storage
 *   >>=? fun state ->
 *   State.Chain.get state chain_id
 *   >>= function
 *   | Ok chain ->
 *       return chain
 *   | Error _ ->
 *       fail
 *         (Ffi_call_error
 *            (Printf.sprintf
 *               "No 'chain' is present for chain_id: %s!"
 *               (Chain_id.to_b58check chain_id)))
 *
 * let distributed_db (storage : storage_t) =
 *   match storage.distributed_db with
 *   | Some db ->
 *       return db
 *   | None ->
 *       fail (Ffi_call_error "No 'distributed_db' is present!")
 *
 * let get_head_for_chain chain =
 *   Chain.head chain >>= fun b -> Lwt.return (State.Block.hash b, State.Block.header b)
 *
 *
 * let get_head_for_chain_id chain_id =
 *   chain global_storage chain_id >>=? fun chain -> return (get_head_for_chain chain)
 *
 * let get_genesis chain =
 *   Chain.genesis chain >>= fun b -> Lwt.return (State.Block.hash b, State.Block.header b)
 *
 * let read_block chain_id block_header_hash =
 *   chain global_storage chain_id
 *   >>=? fun chain ->
 *   State.Block.read_opt chain block_header_hash
 *   >>= fun b -> return (Option.map State.Block.header b)
 *)
(* TODO: candidate for remove - dont needed anymore *)
(*let resolve_test_chain test_chain_id =*)
(*    match test_chain_id with*)
(*    | None -> Lwt.return None*)
(*    | Some test_chain_id -> begin*)
(*            chain_exn global_storage test_chain_id >>= fun test_chain -> begin*)
(*                let { State.Chain.protocol ; _ } = State.Chain.genesis test_chain in*)
(*                let expiration_date = Option.unopt_exn*)
(*                    (Invalid_argument*)
(*                        (Format.asprintf "Monitor.active_chains: no expiration date for the chain %a" Chain_id.pp test_chain_id))*)
(*                      (State.Chain.expiration test_chain)*)
(*                  in*)
(*                Lwt.return (Some(test_chain_id, protocol, expiration_date))*)
(*            end*)
(*        end*)

let context_index (storage : storage_t) =
  let open Lwt_result_syntax in
  match storage.context_index with
  | Some index -> return index
  | None -> tzfail (Ffi_call_error "No 'context_index' is present!")

let config (storage : storage_t) =
  let open Lwt_result_syntax in
  match storage.config with
  | Some config -> return config
  | None ->
      tzfail
        (Ffi_call_error "Storage has not been initialized, no config available")

(*  ==== UNUSED CODE ====
 *
 * (* Shell: add a sanity disk check and fix at node startup - copied from node.ml *)
 * let check_and_fix_storage_consistency state =
 *   State.Chain.all state
 *   >>= fun chains ->
 *   let rec check_block n chain_state block =
 *     fail_unless (n > 0) Tezos_shell_services.Validation_errors.Bad_data_dir
 *     >>=? fun () ->
 *     State.Block.context_exists block
 *     >>= fun is_context_known ->
 *     if is_context_known then (* Found a known context for the block: setting as consistent head *)
 *       Chain.set_head chain_state block >>=? fun _ -> return_unit
 *     else (* Did not find a known context. Need to backtrack the head up *)
 *       let header = State.Block.header block in
 *       State.Block.read chain_state header.shell.predecessor
 *       >>=? fun pred ->
 *       check_block (n - 1) chain_state pred
 *       >>=? fun () ->
 *       (* Make sure to remove the block only after updating the head *)
 *       State.Block.remove block
 *   in
 *   Seq.iter_es
 *     (fun chain_state -> Chain.head chain_state >>= fun block -> check_block 500 chain_state block)
 *     chains
 *)

(* TODO: candidate for remove - dont needed anymore *)
(*let init data_dir genesis enable_testchain context_raw_inspector =*)
(*    ensure_dir_exists data_dir;*)
(*    let chain_genesis = Ffi_patch_context.init_chain_genesis genesis in*)
(*    context_raw_inspector#mark_header_and_protocol chain_genesis.block chain_genesis.protocol;*)
(*    State.init*)
(*        ~store_root:(Filename.concat data_dir "store")*)
(*        ~context_root:(Filename.concat data_dir "context")*)
(*        ~patch_context:(Ffi_patch_context.patch_context None)*)
(*        ~context_raw_inspector*)
(*        (* ?history_mode *)*)
(*        chain_genesis >>=? begin fun (state, chain, index, _history_mode) ->*)
(*            check_and_fix_storage_consistency state >>=? fun () -> begin*)
(*                global_storage.global_state <- Some state;*)
(*                global_storage.context_index <- Some index;*)
(*                global_storage.enable_testchain <- enable_testchain;*)

(*                (* log all chains in storage *)*)
(*                log_all_chains state;*)

(*                (* distributed_db initialization *)*)
(*                let distributed_db = Distributed_db.create state (P2p_facade.init_faked_p2p) in*)
(*                let _ = Distributed_db.activate distributed_db chain in*)
(*                global_storage.distributed_db <- Some distributed_db;*)

(*                match global_storage.global_state with*)
(*                | None -> raise (Ffi_error (Printf.sprintf "[Storage] For context/store was not initialized correctly - data_dir: %s" data_dir));*)
(*                | Some _ -> ffi_log ~msg:(fun () -> (Printf.sprintf "[Storage] For context/store successfully initialized - data_dir: %s" data_dir));*)

(*                (* return current chain, genesis, current head *)*)
(*                get_genesis chain >>= fun (stored_genesis_hash, stored_genesis_header) -> begin*)
(*                    let requested_genesis_hash = chain_genesis.block in*)

(*                    (* check if acutal/stored genesis equals to requested *)*)
(*                    if not (Block_hash.equal stored_genesis_hash requested_genesis_hash) then begin*)
(*                        raise (Ffi_error*)
(*                                (Printf.sprintf "Store's genesis header found has stored_genesis_hash: %s!, \*)
(*                                    which is not equals to requested genesis_hash: %s, \*)
(*                                    that could means that storage is initialized for a different chain (alphanet, mainnet, zeronet, babylonet), \*)
(*                                    check input genesis if is correct or change data_dir!"*)
(*                                    (Block_hash.to_b58check stored_genesis_hash)*)
(*                                    (Block_hash.to_b58check requested_genesis_hash)*)
(*                                    ));*)
(*                    end;*)

(*                    (* everythings ok - return genesis and current head *)*)
(*                    get_head_for_chain chain >>= fun (current_head_hash, _) -> begin*)
(*                        State.Chain.test chain >>= fun test_chain_id -> begin*)
(*                            resolve_test_chain test_chain_id >>= fun test_chain -> begin*)
(*                                Lwt.return_ok ( *)
(*                                    State.Chain.id chain,*)
(*                                    test_chain,*)
(*                                    stored_genesis_hash,*)
(*                                    stored_genesis_header,*)
(*                                    current_head_hash*)
(*                                )*)
(*                            end*)
(*                        end*)
(*                    end*)
(*                end*)
(*            end*)
(*        end >>= function*)
(*            | Ok result ->*)
(*                context_raw_inspector#clear_markers();*)
(*                Lwt.return result*)
(*            | Error error ->*)
(*                context_raw_inspector#clear_markers();*)
(*                Format.kasprintf Stdlib.failwith "%a" pp_print_trace error*)

type patch_context_key_json = string * Data_encoding.json

let init_chain_genesis genesis_params : Genesis.t =
  let (genesis_time, genesis_block, genesis_protocol) = genesis_params in
  let chain_genesis : Genesis.t =
    {
      time = Time.Protocol.of_notation_exn genesis_time;
      block = Block_hash.of_b58check_exn genesis_block;
      protocol = Protocol_hash.of_b58check_exn genesis_protocol;
    }
  in
  chain_genesis

let prepare_irmin cfg =
  let open Lwt_result_syntax in
  let open Ffi_config.Context in
  match cfg.storage with
  | Irmin_only icfg | Both (icfg, _) ->
      let* () = ensure_dir_exists icfg.data_dir in
      return_some icfg.data_dir
  | Tezedge_only _ -> return_none

let prepare_tezedge cfg =
  let open Lwt_result_syntax in
  let open Ffi_config.Context in
  match cfg.storage with
  | Tezedge_only tcfg | Both (_, tcfg) -> return_some tcfg
  | Irmin_only _ -> return_none

let init_context_stats path =
  match path with
  | None -> false
  | Some path ->
      Context.Timings.initialize path ;
      true

let init_context cfg =
  let open Lwt_result_syntax in
  let {Ffi_config.Context.readonly; context_stats_db_path; _} = cfg in
  let* irmin_config = prepare_irmin cfg in
  let* tezedge_config = prepare_tezedge cfg in
  (* according to state.ml State.init *)
  let collect_stats = init_context_stats context_stats_db_path in
  let chain_genesis = init_chain_genesis cfg.genesis in
  let chain_id = Chain_id.of_block_hash chain_genesis.block in
  (* TODO: what to do with this marking here? *)
  (* Option.iter
     (fun inspector ->
       inspector#mark_header_and_protocol chain_genesis.block chain_genesis.protocol)
     context_raw_inspector ; *)
  let key_json_patch_context =
    cfg.sandbox_json_patch_context
    |> Option.map (fun (key, json) -> (key, Ezjsonm.from_string json))
  in
  let*! context_index =
    Context.init
      ?irmin_config
      ?tezedge_config
      ~readonly
      ~patch_context:
        (Patch_context.patch_context chain_genesis key_json_patch_context)
      ~collect_stats
      (Option.value ~default:"ignored" irmin_config)
  in
  (* prepare commit_genesis promise *)
  let commit_genesis ~chain_id ~time ~protocol =
    Context.commit_genesis context_index ~chain_id ~time ~protocol
  in
  global_storage.config <- Some cfg ;
  global_storage.context_index <- Some context_index ;
  global_storage.enable_testchain <- cfg.enable_testchain ;
  if cfg.commit_genesis then
    let* commit =
      commit_genesis
        ~chain_id
        ~time:chain_genesis.time
        ~protocol:chain_genesis.protocol
    in
    return (chain_genesis.protocol, chain_id, Some commit)
  else return (chain_genesis.protocol, chain_id, None)
