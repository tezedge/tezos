open Tezos_base__TzPervasives
open Ffi_errors
open Ffi_logger

type begin_application_request = {
  chain_id : Chain_id.t;
  pred_header : Block_header.t;
  block_header : Block_header.t;
}

type begin_application_response = {result : string}

type begin_construction_request = {
  chain_id : Chain_id.t;
  (* we need whole header because of update_testchain_status *)
  predecessor : Block_header.t;
  (* predecessor block hash *)
  predecessor_hash : Block_hash.t;
  protocol_data : Bytes.t option;
}

type prevalidator_wrapper = {
  chain_id : Chain_id.t;
  protocol : Protocol_hash.t;
  predecessor : Block_hash.t;
}

type prevalidator_request = {
  prevalidator : prevalidator_wrapper;
  operation_hash : Operation_hash.t;
  operation : Operation.t;
  include_operation_data_json : bool;
}

(* Classification with JSON strings instead of error traces *)
type operation_error_classification =
  [ `Branch_delayed of string
  | `Branch_refused of string
  | `Refused of string
  | `Outdated of string ]

type operation_classification =
  [`Applied | `Prechecked | operation_error_classification]

type classified_operation = {
  classification : operation_classification;
  operation_data_json : string option;
  is_endorsement : bool;
}

(* TODO: move parsed json string to pre-filter once we start using it *)
type validate_operation_result =
  | Unparseable
  | Classified of classified_operation

type validate_operation_response = {
  prevalidator : prevalidator_wrapper;
  operation_hash : Operation_hash.t;
  result : validate_operation_result;
  to_reclassify : (Operation_hash.t * operation_error_classification) option;
  validate_operation_started_at : float;
  parse_operation_started_at : float;
  parse_operation_ended_at : float;
  validate_operation_ended_at : float;
}

type pre_filter_operation_result =
  [`Unparseable | `Drop | `High | `Medium | `Low of string list]

type pre_filter_operation_response = {
  prevalidator : prevalidator_wrapper;
  operation_hash : Operation_hash.t;
  operation_data_json : string option;
  result : pre_filter_operation_result;
  pre_filter_operation_started_at : float;
  parse_operation_started_at : float;
  parse_operation_ended_at : float;
  pre_filter_operation_ended_at : float;
}

let trace_to_json trace =
  let json = Data_encoding.Json.construct Error_monad.trace_encoding trace in
  Data_encoding.Json.to_string json

let jsonify_classification_errors
    (classification : Ffi_prevalidator_classification.error_classification) =
  match classification with
  | `Branch_delayed tztrace -> `Branch_delayed (trace_to_json tztrace)
  | `Branch_refused tztrace -> `Branch_refused (trace_to_json tztrace)
  | `Refused tztrace -> `Refused (trace_to_json tztrace)
  | `Outdated tztrace -> `Outdated (trace_to_json tztrace)

let jsonify_classification
    (classification : Ffi_prevalidator_classification.classification) =
  match classification with
  | `Applied -> `Applied
  | `Prechecked -> `Prechecked
  | #Ffi_prevalidator_classification.error_classification as err ->
      jsonify_classification_errors err

let prevalidator_wrapper_encoding =
  let open Data_encoding in
  def
    "Validate_block.prevalidator_wrapper_encoding"
    ~title:"prevalidator_wrapper_encoding"
    ~description:"prevalidator_wrapper_encoding"
  @@ conv
       (fun {chain_id; protocol; predecessor} ->
         (chain_id, protocol, predecessor))
       (fun (chain_id, protocol, predecessor) ->
         {chain_id; protocol; predecessor})
       (obj3
          (req "chain_id" Chain_id.encoding)
          (req "protocol" Protocol_hash.encoding)
          (req "predecessor" Block_hash.encoding))

let pre_filter_operation_response_encoding =
  let open Data_encoding in
  def
    "Validate_block.pre_filter_operation_response"
    ~title:"pre_filter_operation_response params"
    ~description:"Pre-filter operation response"
  @@ conv
       (fun pre_filter_operation_response ->
         let {
           prevalidator;
           operation_hash;
           operation_data_json;
           result = _;
           pre_filter_operation_started_at;
           parse_operation_started_at;
           parse_operation_ended_at;
           pre_filter_operation_ended_at;
         } =
           pre_filter_operation_response
         in
         ( prevalidator,
           operation_hash,
           operation_data_json,
           (*result,*)
           pre_filter_operation_started_at,
           parse_operation_started_at,
           parse_operation_ended_at,
           pre_filter_operation_ended_at ))
       (fun ( prevalidator,
              operation_hash,
              operation_data_json,
              (* result, *)
            pre_filter_operation_started_at,
              parse_operation_started_at,
              parse_operation_ended_at,
              pre_filter_operation_ended_at ) ->
         {
           prevalidator;
           operation_hash;
           operation_data_json;
           result = `Unparseable;
           (* TODO *)
           pre_filter_operation_started_at;
           parse_operation_started_at;
           parse_operation_ended_at;
           pre_filter_operation_ended_at;
         })
       (obj7
          (req "prevalidator" prevalidator_wrapper_encoding)
          (req "operation_hash" Operation_hash.encoding)
          (req "operation_data_json" (option string))
          (* TODO: result *)
          (req "pre_filter_operation_started_at" float)
          (req "parse_operation_started_at" float)
          (req "parse_operation_ended_at" float)
          (req "pre_filter_operation_ended_at" float))

let validate_operation_response_encoding =
  let open Data_encoding in
  def
    "Validate_block.validate_operation_response"
    ~title:"validate_operation_response params"
    ~description:"Validate operation response"
  @@ conv
       (fun validate_operation_response ->
         let {
           prevalidator;
           operation_hash;
           result = _;
           to_reclassify = _;
           validate_operation_started_at;
           parse_operation_started_at;
           parse_operation_ended_at;
           validate_operation_ended_at;
         } =
           validate_operation_response
         in
         ( prevalidator,
           operation_hash,
           validate_operation_started_at,
           parse_operation_started_at,
           parse_operation_ended_at,
           validate_operation_ended_at ))
       (fun ( prevalidator,
              operation_hash,
              validate_operation_started_at,
              parse_operation_started_at,
              parse_operation_ended_at,
              validate_operation_ended_at ) ->
         {
           prevalidator;
           operation_hash;
           result = Unparseable;
           to_reclassify = None;
           (* TODO *)
           validate_operation_started_at;
           parse_operation_started_at;
           parse_operation_ended_at;
           validate_operation_ended_at;
         })
       (obj6
          (req "prevalidator" prevalidator_wrapper_encoding)
          (req "operation_hash" Operation_hash.encoding)
          (* TODO: result *)
          (req "validate_operation_started_at" float)
          (req "parse_operation_started_at" float)
          (req "parse_operation_ended_at" float)
          (req "validate_operation_ended_at" float))

(* We post-process the prefilter result to conver the quotients into strings,
   so that we can send them through FFI+IPC easily *)
let post_process_prefilter_result = function
  | `Drop -> `Drop
  | `High -> `High
  | `Medium -> `Medium
  | `Low qs -> `Low (List.map Q.to_string qs)

let post_process_precheck_result = function
  | `Fail err -> `Fail (jsonify_classification_errors err)
  | `Undecided -> `Undecided
  | `Passed_precheck `No_replace -> `Passed_precheck `No_replace
  | `Passed_precheck (`Replace (op_hash, error_classification)) ->
      `Passed_precheck
        (`Replace (op_hash, jsonify_classification_errors error_classification))

let begin_application (begin_application_request : begin_application_request) =
  let open Lwt_result_syntax in
  let {chain_id; pred_header; block_header} = begin_application_request in
  let predecessor_context_hash = pred_header.shell.context in
  let storage = Storage_facade.global_storage in
  let* context_index = Storage_facade.context_index storage in
  let*! predecessor_block_context =
    Context.checkout context_index predecessor_context_hash
  in
  let* predecessor_block_context =
    match predecessor_block_context with
    | Some predecessor_block_context -> return predecessor_block_context
    | None ->
        tzfail
          (Unknown_predecessor_context
             (Context_hash.to_b58check predecessor_context_hash))
  in
  let*! protocol_hash_from_context =
    Context.get_protocol predecessor_block_context
  in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Begin_application] Resolved predecessor protocol from context: %s"
        (Protocol_hash.to_b58check protocol_hash_from_context)) ;
  (* protocol instance *)
  let* (module Proto_by_predecessor) =
    match Registered_protocol.get protocol_hash_from_context with
    | None ->
        tzfail
          (Ffi_call_error
             ("Unavailable_protocol "
             ^ Protocol_hash.to_b58check protocol_hash_from_context))
    | Some proto -> return proto
  in
  (* prepare block_header and evaluate protocol_data *)
  let block_header_by_protocol =
    ({
       shell = block_header.shell;
       protocol_data =
         Ffi_utils.decode_from_mbytes
           Proto_by_predecessor.block_header_data_encoding
           block_header.protocol_data;
     }
      : Proto_by_predecessor.block_header)
  in
  (* block_validation.apply *)
  let predecessor_block_context =
    Shell_context.wrap_disk_context predecessor_block_context
  in
  let cache =
    match !Protocol_cache.cache with
    | None -> `Load
    | Some cache -> `Inherited (cache, predecessor_context_hash)
  in
  let* _ =
    Proto_by_predecessor.begin_application
      ~chain_id
      ~predecessor_context:predecessor_block_context
      ~predecessor_timestamp:pred_header.shell.timestamp
      ~predecessor_fitness:pred_header.shell.fitness
      ~cache
      block_header_by_protocol
  in
  (* if we came here, lets consider that everything is ok *)
  return {result = "OK"}

let safe_get_prevalidator_filter hash =
  let open Lwt_result_syntax in
  match Prevalidator_filters.find hash with
  | Some filter -> return filter
  | None -> (
      match Registered_protocol.get hash with
      | None ->
          (* This should not happen: it should be handled in the validator. *)
          Error_monad.failwith
            "missing protocol '%a' for the current block."
            Protocol_hash.pp_short
            hash
      | Some protocol ->
          let (module Proto) = protocol in
          let module Filter = Prevalidator_filters.No_filter (Proto) in
          return (module Filter : Prevalidator_filters.FILTER))

let begin_construction (begin_construction_request : begin_construction_request)
    =
  let open Lwt_result_syntax in
  let {chain_id; predecessor; predecessor_hash; protocol_data} =
    begin_construction_request
  in
  let predecessor_context_hash = predecessor.shell.context in
  let storage = Storage_facade.global_storage in
  let* context_index = Storage_facade.context_index storage in
  let*! predecessor_block_context =
    Context.checkout context_index predecessor_context_hash
  in
  let* predecessor_block_context =
    match predecessor_block_context with
    | Some predecessor_block_context -> return predecessor_block_context
    | None ->
        tzfail
          (Unknown_predecessor_context
             (Context_hash.to_b58check predecessor_context_hash))
  in
  let*! protocol_hash_from_context =
    Context.get_protocol predecessor_block_context
  in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Begin_construction] Resolved predecessor protocol from context: %s"
        (Protocol_hash.to_b58check protocol_hash_from_context)) ;
  (* protocol instance *)
  let* (module Filter) =
    safe_get_prevalidator_filter protocol_hash_from_context
  in
  let*! (module Prevalidator) =
    Ffi_prevalidator.create (module Filter) chain_id
  in
  (* start protocol prevalidator *)
  let* () =
    Prevalidator.launch
      ?protocol_data
      ~chain_id
      ~predecessor_context:predecessor_block_context
      ~predecessor_header:predecessor
      ~predecessor_hash
      ()
  in
  return
    {
      chain_id;
      protocol = protocol_hash_from_context;
      predecessor = predecessor_hash;
    }

let log_operation_data_json_failure trace =
  (* Sometimes we cannot produce the JSON for the operation data even though it was
     parsed successfuly because the contract code and params use lazy encoding
     that is not handled during parsing but that will fail later when trying
     to encode the unparseable operation to JSON *)
  Format.eprintf
    "WARN: 'Prevalidator.operation_data_json' call failed with: %a\n%!"
    Error_monad.pp_print_trace
    trace

let validate_operation validate_operation_request =
  let open Lwt_result_syntax in
  let validate_operation_started_at = Unix.gettimeofday () in
  let ({
         prevalidator = prevalidator_wrapper;
         operation_hash;
         operation;
         include_operation_data_json;
       }
        : prevalidator_request) =
    validate_operation_request
  in
  (* find prevalidator *)
  let* (module Filter) =
    safe_get_prevalidator_filter prevalidator_wrapper.protocol
  in
  let*! (module Prevalidator) =
    Ffi_prevalidator.create (module Filter) prevalidator_wrapper.chain_id
  in
  let parse_operation_started_at = Unix.gettimeofday () in
  let parsing_result = Prevalidator.parse operation_hash operation in
  let parse_operation_ended_at = Unix.gettimeofday () in
  let* (result, to_reclassify) =
    match parsing_result with
    | Error _err -> return (Unparseable, None)
    | Ok parsed_operation ->
        let* (classification, is_endorsement, to_reclassify) =
          Prevalidator.validate_operation parsed_operation
        in
        let classification = jsonify_classification classification in
        let to_reclassify =
          Option.map
            (fun (oph, c) -> (oph, jsonify_classification_errors c))
            to_reclassify
        in
        if include_operation_data_json then
          match Prevalidator.operation_data_json parsed_operation with
          | Error trace ->
              log_operation_data_json_failure trace ;
              return (Unparseable, to_reclassify)
          | Ok operation_data_json ->
              let operation_data_json = Some operation_data_json in
              return
                ( Classified
                    {classification; operation_data_json; is_endorsement},
                  to_reclassify )
        else
          return
            ( Classified
                {classification; operation_data_json = None; is_endorsement},
              to_reclassify )
  in
  let validate_operation_ended_at = Unix.gettimeofday () in
  let response : validate_operation_response =
    {
      prevalidator = prevalidator_wrapper;
      operation_hash;
      result;
      to_reclassify;
      validate_operation_started_at;
      parse_operation_started_at;
      parse_operation_ended_at;
      validate_operation_ended_at;
    }
  in
  return response

let pre_filter_operation pre_filter_operation_request =
  let open Lwt_result_syntax in
  let pre_filter_operation_started_at = Unix.gettimeofday () in
  let ({
         prevalidator = prevalidator_wrapper;
         operation_hash;
         operation;
         include_operation_data_json;
       }
        : prevalidator_request) =
    pre_filter_operation_request
  in
  (* find prevalidator *)
  let* (module Filter) =
    safe_get_prevalidator_filter prevalidator_wrapper.protocol
  in
  let*! (module Prevalidator) =
    Ffi_prevalidator.create (module Filter) prevalidator_wrapper.chain_id
  in
  let parse_operation_started_at = Unix.gettimeofday () in
  let parsing_result = Prevalidator.parse operation_hash operation in
  let parse_operation_ended_at = Unix.gettimeofday () in
  let* (result, operation_data_json) =
    match parsing_result with
    | Error _err -> return (`Unparseable, None)
    | Ok parsed_operation ->
        let* result = Prevalidator.pre_filter_operation parsed_operation in
        if include_operation_data_json then
          match Prevalidator.operation_data_json parsed_operation with
          | Error trace ->
              log_operation_data_json_failure trace ;
              return (`Unparseable, None)
          | Ok operation_data_json ->
              return
                (post_process_prefilter_result result, Some operation_data_json)
        else return (post_process_prefilter_result result, None)
  in
  let pre_filter_operation_ended_at = Unix.gettimeofday () in
  let response : pre_filter_operation_response =
    {
      prevalidator = prevalidator_wrapper;
      operation_hash;
      operation_data_json;
      result;
      pre_filter_operation_started_at;
      parse_operation_started_at;
      parse_operation_ended_at;
      pre_filter_operation_ended_at;
    }
  in
  return response
