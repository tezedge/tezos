(* Helper code for ffi_block_validation.ml *)

type operation_metadata = Metadata of Bytes.t | Too_large_metadata

type ops_metadata =
  | No_metadata_hash of operation_metadata list list
  | Metadata_hash of (operation_metadata * Operation_metadata_hash.t) list list

type apply_block_timestamps = {
  mutable apply_start_t : float;
  mutable operations_decoding_start_t : float;
  mutable operations_decoding_end_t : float;
  mutable operations_application_timestamps : (float * float) list list;
  mutable operations_metadata_encoding_start_t : float;
  mutable operations_metadata_encoding_end_t : float;
  mutable begin_application_start_t : float;
  mutable begin_application_end_t : float;
  mutable finalize_block_start_t : float;
  mutable finalize_block_end_t : float;
  mutable collect_new_rolls_owner_snapshots_start_t : float;
  mutable collect_new_rolls_owner_snapshots_end_t : float;
  mutable commit_start_t : float;
  mutable commit_end_t : float;
  mutable apply_end_t : float;
}

let empty_apply_block_timestamps () =
  {
    apply_start_t = 0.0;
    operations_decoding_start_t = 0.0;
    operations_decoding_end_t = 0.0;
    operations_application_timestamps = [];
    operations_metadata_encoding_start_t = 0.0;
    operations_metadata_encoding_end_t = 0.0;
    begin_application_start_t = 0.0;
    begin_application_end_t = 0.0;
    finalize_block_start_t = 0.0;
    finalize_block_end_t = 0.0;
    collect_new_rolls_owner_snapshots_start_t = 0.0;
    collect_new_rolls_owner_snapshots_end_t = 0.0;
    commit_start_t = 0.0;
    commit_end_t = 0.0;
    apply_end_t = 0.0;
  }

(** Frozen snapshot of roll owners for a given cycle *)
type cycle_rolls_owner_snapshot = {
  cycle : int;  (** cycle for which this data belongs *)
  seed_bytes : bytes;  (** randomness seed byte encoded *)
  rolls_data : (bytes * int list) list;
      (** snapshot data as a list of (public key * slots),
            with public key encoded in binary form *)
  last_roll : int32;  (** last roll number *)
}

type forking_testchain_data = {
  forking_block_hash : Block_hash.t;
  test_chain_id : Chain_id.t;
}

type apply_block_response = {
  validation_result_message : string;
  context_hash : Context_hash.t;
  protocol_hash : Protocol_hash.t;
  next_protocol_hash : Protocol_hash.t;
  block_header_proto_json : string;
  block_header_metadata_bytes : bytes;
  operations_metadata_bytes : bytes list list;
  max_operations_ttl : int;
  last_allowed_fork_level : int32;
  forking_testchain : bool;
  forking_testchain_data : forking_testchain_data option;
  block_metadata_hash : Block_metadata_hash.t option;
  ops_metadata_hashes : Operation_metadata_hash.t list list option;
  (* Note: this is calculated from ops_metadata_hashes - we need this in request *)
  ops_metadata_hash : Operation_metadata_list_list_hash.t option;
  cycle : int option;
  cycle_position : int option;
  cycle_rolls_owner_snapshots : cycle_rolls_owner_snapshot list;
  new_protocol_constants_json : string option;
  new_cycle_eras_json : string option;
  commit_time : float;
  execution_timestamps : apply_block_timestamps;
}

type apply_block_result = {
  response : apply_block_response;
  timestamp : Time.Protocol.t;
  cache : Environment_context.Context.cache;
}

module Tezedge (Proto : Registered_protocol.T) = struct
  (* TODO - TE-210: temporary solution until checkpoints have been implemented *)
  (* Since we cannot access the cycle position directly from the header,
     the solution is to use the encoding definition to conver it into a
     JSON representation and get the value from there *)
  let extract_level_info block_metadata_json field =
    try
      Some
        (Json_query.query
           [`Field "level_info"; `Field field]
           block_metadata_json)
    with Not_found -> (
      try
        Some
          (Json_query.query [`Field "level"; `Field field] block_metadata_json)
      with Not_found -> None)

  let extract_cycle_position encoding block_metadata =
    let block_metadata_json =
      Data_encoding.Json.construct encoding block_metadata
    in
    match extract_level_info block_metadata_json "cycle_position" with
    | Some (`Float n) -> Some (int_of_float n)
    | _ -> None

  (* Extracts current cycle from block metadata *)
  let extract_cycle encoding block_metadata =
    let block_metadata_json =
      Data_encoding.Json.construct encoding block_metadata
    in
    match extract_level_info block_metadata_json "cycle" with
    | Some (`Float n) -> Some (int_of_float n)
    | _ -> None

  (* Decodes a roll as an int32 for the specified protocol *)
  let protocol_roll_to_int32 protocol_hash roll =
    let open Lwt_result_syntax in
    let open Data_encoding.Binary in
    match Tezos_crypto.Protocol_hash.to_b58check protocol_hash with
    | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY" ->
        return
          Tezos_protocol_001_PtCJ7pwo.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt" ->
        return
          Tezos_protocol_002_PsYLVpVv.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP" ->
        return
          Tezos_protocol_003_PsddFKi3.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd" ->
        return
          Tezos_protocol_004_Pt24m4xi.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU" ->
        return
          Tezos_protocol_005_PsBABY5H.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS" ->
        return
          Tezos_protocol_005_PsBabyM1.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb" ->
        return
          Tezos_protocol_006_PsCARTHA.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo" ->
        return
          Tezos_protocol_007_PsDELPH1.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq" ->
        return
          Tezos_protocol_008_PtEdoTez.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA" ->
        return
          Tezos_protocol_008_PtEdo2Zk.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i" ->
        return
          Tezos_protocol_009_PsFLoren.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
        return
          Tezos_protocol_010_PtGRANAD.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx" ->
        return
          Tezos_protocol_011_PtHangz2.Protocol.Roll_repr.(
            of_bytes_exn encoding roll |> to_int32)
    | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A" ->
        (* No rolls after proto 011 *)
        return (Int32.of_int 0)
    | "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY" ->
        (* No rolls after proto 011 *)
        return (Int32.of_int 0)
    | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
        return (Int32.of_int 0)
    | other ->
        failwith "Protocol not supported (protocol_roll_to_int32): %s" other

  let accumulate_rolls_data depth key tree acc =
    let open Lwt_syntax in
    match (key, depth) with
    | ([roll_num], 1) | ([_; _; roll_num], 3) -> (
        let* value = Context.Tree.to_value tree in
        match value with
        | None -> return acc
        | Some value ->
            (* FIXME: what if it cannot be parsed? *)
            let roll_num = int_of_string roll_num in
            let public_key =
              Data_encoding.Binary.of_bytes_exn
                Signature.Public_key.encoding
                value
            in
            return ((public_key, roll_num) :: acc))
    | _ -> return acc

  (* Group keys with the roll nums assigned to them so that:
     [(pk1, num1); (pk2, num3); ...] results in:
     [(pk, [num1; num2; ...]); (pk2, [...]); ...] *)
  let group_rolls_data rolls_data =
    let compare (lkey, _) (rkey, _) = Signature.Public_key.compare lkey rkey in
    let sorted = List.fast_sort compare rolls_data in
    let group_adjacent acc data =
      match (acc, data) with
      | ([], (next_pk, num)) -> [(next_pk, [num])]
      | ((curr_pk, curr_nums) :: acc, (next_pk, num)) ->
          if Signature.Public_key.equal curr_pk next_pk then
            (curr_pk, num :: curr_nums) :: acc
          else (next_pk, [num]) :: (curr_pk, curr_nums) :: acc
    in
    List.fold_left group_adjacent [] sorted

  let unwrap_value name value =
    let open Lwt_result_syntax in
    match value with
    | Some x -> return x
    | None -> failwith "no value in: %s" name

  (* Before protocol H 3 nesting levels were used, in H the context flattening
     happened and now the immediate children are the values we want *)
  let roll_snapshots_depth protocol_hash =
    match Protocol_hash.to_b58check protocol_hash with
    | "Ps6mwMrF2ER2s51cp9yYpjDcuzQjsc2yAz8bQsRgdaRxw4Fk95H"
    | "PtBMwNZT94N7gXKw4i273CKcSaBrrBnqnt3RATExNKr9KNX2USV"
    | "PtYuensgYBb3G3x1hLLbCmcav8ue8Kyd2khADcL5LsT5R1hcXex"
    | "ProtoGenesisGenesisGenesisGenesisGenesisGenesk612im"
    | "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P"
    | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
    | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
    | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
    | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
    | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
    | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
    | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
    | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
    | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
    | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
    | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
    | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
        3
    | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx"
    | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A"
    | "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY"
    | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
        1
    | other ->
        Printf.eprintf
          "[WARNING] `roll_snapshots_depth` is not aware of protocol %s, \
           defaulting to 3\n\
           %!"
          other ;
        3
  (* TODO: should default to 1, because it is what future protocols will use *)

  let collect_rolls_data_for_cycle protocol_hash context cycle =
    let open Lwt_result_syntax in
    let cycle_key = string_of_int cycle in
    let*! roll_snapshot =
      Context.find context ["cycle"; cycle_key; "roll_snapshot"]
    in
    let* roll_snapshot = unwrap_value "roll_snapshot" roll_snapshot in
    let*! random_seed =
      Context.find context ["cycle"; cycle_key; "random_seed"]
    in
    let* random_seed = unwrap_value "random_seed" random_seed in
    (* FIXME: what if the encoding changes? can that happen? *)
    let roll_snapshot =
      Data_encoding.Binary.of_bytes_exn Data_encoding.uint16 roll_snapshot
    in
    let*! last_roll =
      Context.find
        context
        ["cycle"; cycle_key; "last_roll"; string_of_int roll_snapshot]
    in
    let* last_roll = unwrap_value "last_roll" last_roll in
    let* last_roll = protocol_roll_to_int32 protocol_hash last_roll in
    let depth = roll_snapshots_depth protocol_hash in
    let*! rolls_data =
      Context.fold
        context
        ["rolls"; "owner"; "snapshot"; cycle_key; string_of_int roll_snapshot]
        ~order:`Undefined
        ~depth:(`Eq depth)
        ~init:[]
        ~f:(accumulate_rolls_data depth)
    in
    let rolls_data = group_rolls_data rolls_data in
    let rolls_data =
      List.map
        (fun (pk, data) ->
          ( Data_encoding.Binary.to_bytes_exn Signature.Public_key.encoding pk,
            data ))
        rolls_data
    in
    return {cycle; seed_bytes = random_seed; rolls_data; last_roll}

  let protocol_constants_json caller protocol_hash context =
    let open Lwt_result_syntax in
    let of_bytes_exn = Data_encoding.Binary.of_bytes_exn in
    let construct_json = Data_encoding.Json.construct in
    match Tezos_crypto.Protocol_hash.to_b58check protocol_hash with
    | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY" ->
        let open Tezos_protocol_001_PtCJ7pwo.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Parameters_repr.constants_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt" ->
        let open Tezos_protocol_002_PsYLVpVv.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Parameters_repr.constants_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP" ->
        let open Tezos_protocol_003_PsddFKi3.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Parameters_repr.constants_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd" ->
        let open Tezos_protocol_004_Pt24m4xi.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Parameters_repr.constants_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU" ->
        let open Tezos_protocol_005_PsBABY5H.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS" ->
        let open Tezos_protocol_005_PsBabyM1.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb" ->
        let open Tezos_protocol_006_PsCARTHA.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo" ->
        let open Tezos_protocol_007_PsDELPH1.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq" ->
        let open Tezos_protocol_008_PtEdoTez.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA" ->
        let open Tezos_protocol_008_PtEdo2Zk.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i" ->
        let open Tezos_protocol_009_PsFLoren.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
        let open Tezos_protocol_010_PtGRANAD.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.{fixed; parametric} in
        return @@ construct_json Constants_repr.encoding constants
    | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx" ->
        let open Tezos_protocol_011_PtHangz2.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.all parametric in
        return @@ construct_json Constants_repr.encoding constants
    | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A" ->
        let open Tezos_protocol_012_Psithaca.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.all parametric in
        return @@ construct_json Constants_repr.encoding constants
    | "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY" ->
        let open Tezos_protocol_013_PtJakart.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.all_of_parametric parametric in
        return @@ construct_json Constants_repr.encoding constants
    (*| "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
        let open Tezos_protocol_alpha.Protocol in
        let*! constants = Context.find context ["v1"; "constants"] in
        let* constants = unwrap_value "constants" constants in
        let parametric =
          of_bytes_exn Constants_repr.parametric_encoding constants
        in
        let constants = Constants_repr.all parametric in
        return @@ construct_json Constants_repr.encoding constants*)
    | other ->
        failwith
          "Protocol not supported (%s -> protocol_constants_json): %s"
          caller
          other

  let protocol_cycle_eras_json protocol_hash context =
    let open Lwt_result_syntax in
    let of_bytes_exn = Data_encoding.Binary.of_bytes_exn in
    let construct_json = Data_encoding.Json.construct in
    match Tezos_crypto.Protocol_hash.to_b58check protocol_hash with
    | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
    | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
    | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
    | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
    | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
    | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
    | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
    | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
    | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
    | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
    | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i" ->
        return_none
    | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
        let open Tezos_protocol_010_PtGRANAD.Protocol in
        let*! cycle_eras = Context.find context ["v1"; "cycle_eras"] in
        let* cycle_eras = unwrap_value "cycle_eras" cycle_eras in
        let cycle_eras =
          of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras
        in
        return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
    | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx" ->
        let open Tezos_protocol_011_PtHangz2.Protocol in
        let*! cycle_eras = Context.find context ["v1"; "cycle_eras"] in
        let* cycle_eras = unwrap_value "cycle_eras" cycle_eras in
        let cycle_eras =
          of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras
        in
        return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
    | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A" ->
        let open Tezos_protocol_012_Psithaca.Protocol in
        let*! cycle_eras = Context.find context ["v1"; "cycle_eras"] in
        let* cycle_eras = unwrap_value "cycle_eras" cycle_eras in
        let cycle_eras =
          of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras
        in
        return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
    | "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY" ->
        let open Tezos_protocol_013_PtJakart.Protocol in
        let*! cycle_eras = Context.find context ["v1"; "cycle_eras"] in
        let* cycle_eras = unwrap_value "cycle_eras" cycle_eras in
        let cycle_eras =
          of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras
        in
        return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
    | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
        (* TODO - TE-706: should come from alpha, but somehow it is not up to date *)
        return_none
    | other ->
        failwith "Protocol not supported (protocol_cycle_eras_json): %s" other

  let protocol_preserved_cycles protocol_hash context =
    let open Lwt_result_syntax in
    let* constants_json =
      protocol_constants_json "protocol_preserved_cycles" protocol_hash context
    in
    return
      (Ezjsonm.get_int
         (Json_query.query [`Field "preserved_cycles"] constants_json))

  let rec ( --> ) i j =
    (* [i; i+1; ...; j] *)
    if Compare.Int.(i > j) then [] else i :: (succ i --> j)

  let rec collect_cycles_rolls_data protocol_hash context cycles acc =
    let open Lwt_result_syntax in
    match cycles with
    | [] -> return acc
    | cycle :: cycles ->
        let* data = collect_rolls_data_for_cycle protocol_hash context cycle in
        collect_cycles_rolls_data protocol_hash context cycles (data :: acc)

  let protocols_without_rolls =
    [
      Tezos_protocol_012_Psithaca.Protocol.hash;
      Tezos_protocol_013_PtJakart.Protocol.hash;
    ]

  (* Starting with 012 Ithaca, rolls data in snapshots has been substituted by stakes data *)
  let protocol_has_rolls protocol_hash =
    not
    @@ List.mem ~equal:Protocol_hash.equal protocol_hash protocols_without_rolls

  let collect_new_rolls_owner_snapshots protocol_hash context next_cycle
      next_cycle_position level =
    let open Lwt_result_syntax in
    match (next_cycle, next_cycle_position, level) with
    | _ when not (protocol_has_rolls protocol_hash) -> return []
    | (Some next_cycle, Some 0, _) ->
        (* Only at position 0 of a cycle we collect the new data *)
        let* preserved_cycles =
          protocol_preserved_cycles protocol_hash context
        in
        let new_frozen_cycle = next_cycle + preserved_cycles in
        let* data =
          collect_rolls_data_for_cycle protocol_hash context new_frozen_cycle
        in
        return [data]
    | (_, _, 1l) ->
        (* Special handling for level=1 to collect the data computed during storage init *)
        let* preserved_cycles =
          protocol_preserved_cycles protocol_hash context
        in
        let* data =
          collect_cycles_rolls_data
            protocol_hash
            context
            (0 --> preserved_cycles)
            []
        in
        return data
    | (_, None, level) when level > 1l ->
        Printf.eprintf
          "WARNING: cannot collect rolls without knowing the cycle position\n%!" ;
        return []
    | _ -> return []

  let collect_protocol_constants protocol_hash context =
    let open Lwt_result_syntax in
    let* constants_json =
      protocol_constants_json "collect_protocol_constants" protocol_hash context
    in
    return (Data_encoding.Json.to_string constants_json)

  let collect_protocol_cycle_eras_json protocol_hash context =
    let open Lwt_result_syntax in
    let* cycle_eras_json = protocol_cycle_eras_json protocol_hash context in
    return (Option.map Data_encoding.Json.to_string cycle_eras_json)

  let extract_metadata_bytes metadata =
    match metadata with
    | Metadata bytes -> bytes
    | Too_large_metadata -> assert false
  (* TODO: handle limits *)

  let extract_operations_metadata (ops_metadata : ops_metadata) =
    match ops_metadata with
    | No_metadata_hash op_metadata_list_list ->
        op_metadata_list_list |> List.map (List.map extract_metadata_bytes)
    | Metadata_hash op_metadata_list_list ->
        op_metadata_list_list
        |> List.map
             (List.map (fun (metadata, _) -> extract_metadata_bytes metadata))

  let extract_operations_metadata_hashes (ops_metadata : ops_metadata) =
    match ops_metadata with
    | No_metadata_hash _ -> None
    | Metadata_hash op_metadata_list_list ->
        Some (op_metadata_list_list |> List.map (List.map snd))

  let new_protocol_hook context new_protocol =
    let open Lwt_result_syntax in
    if Protocol_hash.equal new_protocol Proto.hash then return (None, None)
    else
      let new_proto_context = Shell_context.unwrap_disk_context context in
      let* new_cycle_eras_json =
        collect_protocol_cycle_eras_json new_protocol new_proto_context
      in
      let* new_constants =
        collect_protocol_constants new_protocol new_proto_context
      in
      return (new_cycle_eras_json, Some new_constants)

  let before_commit_hook protocol_hash ctxt (block_header : Proto.block_header)
      block_metadata (execution_timestamps : apply_block_timestamps) =
    let open Lwt_result_syntax in
    let cycle_position =
      extract_cycle_position Proto.block_header_metadata_encoding block_metadata
    in
    let cycle =
      extract_cycle Proto.block_header_metadata_encoding block_metadata
    in
    (* First notify of the application before the commit, with no hash. *)
    Context.block_applied
      (Context.index ctxt)
      None
      block_header.shell.level
      cycle_position ;
    execution_timestamps.collect_new_rolls_owner_snapshots_start_t <-
      Unix.gettimeofday () ;
    let* cycle_rolls_owner_snapshots =
      collect_new_rolls_owner_snapshots
        protocol_hash
        ctxt
        cycle
        cycle_position
        block_header.shell.level
    in
    execution_timestamps.collect_new_rolls_owner_snapshots_end_t <-
      Unix.gettimeofday () ;
    return (cycle, cycle_position, cycle_rolls_owner_snapshots)

  let after_commit_hook ctxt context_hash cycle_position forking_testchain
      (block_header : Proto.block_header) block_header_hash new_protocol =
    let open Lwt_result_syntax in
    Context.block_applied
      (Context.index ctxt)
      (Some context_hash)
      block_header.shell.level
      cycle_position ;
    Context.Timings.mark_block None ;
    let block_header_proto_json =
      Ffi_utils.to_json
        ~newline:false
        ~minify:true
        Proto.block_header_data_encoding
        block_header.protocol_data
    in
    (* originally, this is done by Distributed_db.commit_block -> state.ml *)
    let forking_testchain_data : forking_testchain_data option =
      if forking_testchain then
        let test_chain_genesis =
          Context.compute_testchain_genesis block_header_hash
        in
        let test_chain_id =
          Context.compute_testchain_chain_id test_chain_genesis
        in
        Some {forking_block_hash = test_chain_genesis; test_chain_id}
      else None
    in
    if not (Protocol_hash.equal Proto.hash new_protocol) then
      Context.Timings.mark_protocol new_protocol ;
    return (forking_testchain_data, block_header_proto_json)

  let is_testchain_forking ctxt =
    let open Lwt_syntax in
    let* result = Context.get_test_chain ctxt in
    match result with
    | Not_running | Running _ -> Lwt.return_false
    | Forking _ -> Lwt.return_true
end