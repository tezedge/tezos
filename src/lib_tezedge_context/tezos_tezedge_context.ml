module Context_binary = Tezos_irmin_context.Context_binary
module Context = Xcontext
module Xcontext = Xcontext
module Context_dump = Tezos_irmin_context.Context_dump
module Tcontext = Tcontext
module Icontext = Icontext
module Errors = Errors
