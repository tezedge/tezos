
external default_thread_name : unit -> unit = "set_main_thread_name"
external dump : unit -> unit = "call_tezedge_gcov_dump"

#ifdef FUZZING_BISECT
let register_dump_callback () = Bisect.Runtime.set_dump_callback dump
#else
let register_dump_callback () = ()
#endif
