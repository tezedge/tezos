#define CAML_NAME_SPACE
#include <caml/mlvalues.h>

#ifdef __linux__
# include <sys/prctl.h>
#endif

value set_main_thread_name(value unit)
{
#ifdef __linux__
  prctl(PR_SET_NAME, (unsigned long)"main", 0, 0, 0);
#endif
  return unit;
}

/* GCOV fucntions */
typedef value (*ml_tezedge_gcov_dump_f)(value);
static ml_tezedge_gcov_dump_f ml_tezedge_gcov_dump = NULL;

value call_tezedge_gcov_dump(value unit)
{
  if (ml_tezedge_gcov_dump)
    return ml_tezedge_gcov_dump(unit);

  return unit;
}

void initialize_tezedge_gcov_callbacks(ml_tezedge_gcov_dump_f ml_tezedge_gcov_dump_ptr)
{
  ml_tezedge_gcov_dump = ml_tezedge_gcov_dump_ptr;
}
