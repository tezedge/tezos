

;
;        /!\ /!\ Do not modify this file /!\ /!\
;
; but the original template in `tezos-protocol-compiler`
;


(rule
 (targets environment.ml)
 (action
  (write-file %{targets}
              "module Name = struct let name = \"genesis-PtBMwNZT\" end
include Tezos_protocol_environment.MakeV0(Name)()
module CamlinternalFormatBasics = struct include CamlinternalFormatBasics end
")))

(rule
 (targets registerer.ml)
 (deps
   data.ml
   services.ml
   main.mli main.ml
   (:src_dir TEZOS_PROTOCOL))
 (action
  (with-stdout-to %{targets}
                  (chdir %{workspace_root} (run %{bin:tezos-embedded-protocol-packer} "%{src_dir}" "genesis_PtBMwNZT")))))

(rule
 (targets functor.ml)
 (deps
   data.ml
   services.ml
   main.mli main.ml
   (:src_dir TEZOS_PROTOCOL))
 (action (with-stdout-to %{targets}
                         (chdir %{workspace_root}
                                (run %{bin:tezos-protocol-compiler.tezos-protocol-packer} %{src_dir})))))

(rule
 (targets protocol.ml)
 (deps
   data.ml
   services.ml
   main.mli main.ml)
 (action
  (write-file %{targets}
    "module Environment = Tezos_protocol_environment_genesis_PtBMwNZT.Environment
let hash = Tezos_crypto.Protocol_hash.of_b58check_exn \"PtBMwNZT94N7gXKw4i273CKcSaBrrBnqnt3RATExNKr9KNX2USV\"
let name = Environment.Name.name
include Tezos_raw_protocol_genesis_PtBMwNZT
include Tezos_raw_protocol_genesis_PtBMwNZT.Main
")))

(library
 (name tezos_protocol_environment_genesis_PtBMwNZT)
 (public_name tezos-protocol-genesis-PtBMwNZT.environment)
 (library_flags (:standard -linkall))
 (libraries tezos-protocol-environment)
 (modules Environment))

(library
 (name tezos_raw_protocol_genesis_PtBMwNZT)
 (public_name tezos-protocol-genesis-PtBMwNZT.raw)
 (libraries tezos_protocol_environment_genesis_PtBMwNZT)
 (library_flags (:standard -linkall))
 (flags (:standard -nopervasives -nostdlib
                   -w +a-4-6-7-9-29-32-40..42-44-45-48
                   -warn-error +a
                   -open Tezos_protocol_environment_genesis_PtBMwNZT__Environment
                   -open Pervasives
                   -open Error_monad))
 (modules
   Data
   Services
   Main))

(install
 (section lib)
 (package tezos-protocol-genesis-PtBMwNZT)
 (files (TEZOS_PROTOCOL as raw/TEZOS_PROTOCOL)))

(library
 (name tezos_protocol_genesis_PtBMwNZT)
 (public_name tezos-protocol-genesis-PtBMwNZT)
 (libraries
      tezos-protocol-environment
      tezos-protocol-environment.sigs
      tezos_raw_protocol_genesis_PtBMwNZT)
 (flags -w "+a-4-6-7-9-29-40..42-44-45-48"
        -warn-error "+a"
        -nopervasives)
 (modules Protocol))

(library
 (name tezos_protocol_genesis_PtBMwNZT_functor)
 (public_name tezos-protocol-genesis-PtBMwNZT.functor)
 (libraries
      tezos-protocol-environment
      tezos-protocol-environment.sigs
      tezos_raw_protocol_genesis_PtBMwNZT)
 (flags -w "+a-4-6-7-9-29-40..42-44-45-48"
        -warn-error "+a"
        -nopervasives)
 (modules Functor))

(library
 (name tezos_embedded_protocol_genesis_PtBMwNZT)
 (public_name tezos-embedded-protocol-genesis-PtBMwNZT)
 (library_flags (:standard -linkall))
 (libraries tezos-protocol-genesis-PtBMwNZT
            tezos-protocol-updater
            tezos-protocol-environment)
 (flags (:standard -w +a-4-6-7-9-29-32-40..42-44-45-48
                   -warn-error +a))
 (modules Registerer))

(rule
 (alias runtest_compile_protocol)
 (deps
   data.ml
   services.ml
   main.mli main.ml
  (:src_dir TEZOS_PROTOCOL))
 (action (run %{bin:tezos-protocol-compiler} -no-hash-check .)))

(rule
 (alias runtest_sandbox)
 (deps .tezos_protocol_genesis_PtBMwNZT.objs/native/tezos_protocol_genesis_PtBMwNZT.cmx)
 (action (progn)))

(rule
 (alias runtest)
 (package tezos-protocol-genesis-PtBMwNZT)
 (deps (alias runtest_sandbox))
 (action (progn)))
